import { config } from "../config";

export const environment = {
  production: true,
  apiURL: 'https://evote-epn1.azurewebsites.net/api/'
};
