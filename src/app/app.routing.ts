import { Routes } from '@angular/router';
import { AdminLayoutComponent } from './shared/components/layouts/admin-layout/admin-layout.component';
import { AuthLayoutComponent } from './shared/components/layouts/auth-layout/auth-layout.component';
import { AuthGuard } from './shared/guards/auth.guard';

export const rootRouterConfig: Routes = [
  { 
    path: '', 
    redirectTo: 'sessions/signin',
    pathMatch: 'full' 
  },
  {
    path: '', 
    component: AuthLayoutComponent,
    children: [
      { 
        path: 'sessions', 
        loadChildren: () => import('./views/sessions/sessions.module').then(m => m.SessionsModule),
        data: { title: 'Session'} 
      }
    ]
  },
  {
    path: '',
    component: AdminLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'catalogos',
        loadChildren: () => import('./views/voto-electronico/catalogos/catalogo.module').then(m => m.CatalogoModule),
        data: { title: 'Catalogos', breadcrumb: 'CATALOGOS'}
      }
    ]
  },
  {
    path: '',
    component: AdminLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'configuracion',
        loadChildren: () => import('./views/voto-electronico/configuracion/configuracion.module').then(m => m.ConfiguracionModule),
        data: { title: 'Configuracion', breadcrumb: 'CONFIGURACIÓN'}
      }
    ]
  },
  {
    path: '', 
    component: AdminLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'others', 
        loadChildren: () => import('./views/others/others.module').then(m => m.OthersModule), 
        data: { title: 'Inicio', breadcrumb: 'INICIO'}
      }
    ]
  },
    {
        path: '',
        component: AdminLayoutComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: 'resultados',
                loadChildren: () => import('./views/voto-electronico/resultados/resumen.module').then(m => m.ResumenModule),
                data: { title: 'Resultados',breadcrumb: 'RESULTADOS'}
            }
        ]
    },
  { 
    path: '**', 
    redirectTo: 'others/blank'
  }
];

