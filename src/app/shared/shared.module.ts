import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

// SERVICES
import {ThemeService} from './services/theme.service';
import {NavigationService} from './services/navigation.service';
import {RoutePartsService} from './services/route-parts.service';
import {AuthGuard} from './guards/auth.guard';
import {UserRoleGuard} from './guards/user-role.guard';
import {AppConfirmService} from './services/app-confirm/app-confirm.service';
import {AppLoaderService} from './services/app-loader/app-loader.service';

import {SharedComponentsModule} from './components/shared-components.module';
import {SharedPipesModule} from './pipes/shared-pipes.module';
import {SharedDirectivesModule} from './directives/shared-directives.module';
import {CrudService, CrudServiceNoInterceptor} from './services/facade-services/crud.service';
import {ErrorInterceptorService} from './services/error-interceptor.service';
import {NotificationsStoreService} from './services/notification-store.service';
import {UtilService} from './services/util/util.service';

import {AppVotarService} from './services/app-votar/app-votar.service';
import {MatIconModule} from '@angular/material/icon';
import {RutasDinamicasPorRolGuardGuard} from './guards/rutas-dinamicas-rol-guard';

@NgModule({
    imports: [
        CommonModule,
        SharedComponentsModule,
        SharedPipesModule,
        SharedDirectivesModule,
        MatIconModule,
    ],
    providers: [
        ThemeService,
        NavigationService,
        RoutePartsService,
        AuthGuard,
        UserRoleGuard,
        AppConfirmService,
        AppLoaderService,
        CrudService,
        CrudServiceNoInterceptor,
        ErrorInterceptorService,
        NotificationsStoreService,
        UtilService,
        //propios app voto epn
        AppVotarService,
        RutasDinamicasPorRolGuardGuard
    ],
    exports: [
        SharedComponentsModule,
        SharedPipesModule,
        SharedDirectivesModule,
    ]
})
export class SharedModule {
}
