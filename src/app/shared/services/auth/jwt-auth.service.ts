import {Injectable} from '@angular/core';
import {LocalStoreService} from '../local-store.service';
import {HttpClient} from '@angular/common/http';
import {Router, ActivatedRoute} from '@angular/router';
import {map, catchError, delay} from 'rxjs/operators';
import {User} from '../../models/user.model';
import {of, BehaviorSubject, throwError, Observable} from 'rxjs';
import {environment} from 'environments/environment';
import {CrudService} from '../facade-services/crud.service';
import {Permiso, Sesion} from '../../../views/constantes/apis';
import {MatSnackBar} from '@angular/material/snack-bar';
import {SessionStoreService} from '../session-store.service';
import {UtilService} from '../util/util.service';

// ================= only for demo purpose ===========
// const DEMO_TOKEN =
//     'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YjhkNDc4MDc4NmM3MjE3MjBkYzU1NzMiLCJlbWFpbCI6InJhZmkuYm9ncmFAZ21haWwuY29tIiwicm9sZSI6IlNBIiwiYWN0aXZlIjp0cnVlLCJpYXQiOjE1ODc3MTc2NTgsImV4cCI6MTU4ODMyMjQ1OH0.dXw0ySun5ex98dOzTEk0lkmXJvxg3Qgz4ed';
//
// const DEMO_USER: User = {
//     id: '5b700c45639d2c0c54b354ba',
//     displayName: 'Watssssssssss Joyce',
//     role: 'SA',
// };

// ================= you will get those data from server =======

@Injectable({
    providedIn: 'root',
})
export class JwtAuthService {
    token;
    isAuthenticated: Boolean;
    user: User={};
    user$ = (new BehaviorSubject<User>(this.user));
    signingIn: Boolean;
    return: string;
    JWT_TOKEN = 'token';
    APP_USER = 'EGRET_USER';
    APP_MENU = "MenuItems"

    constructor(
        private ls: LocalStoreService,
        private http: HttpClient,
        private router: Router,
        private route: ActivatedRoute,
        private crudService: CrudService,
        private snack: MatSnackBar,
        private sessionStorage: SessionStoreService,
        private util: UtilService,
    ) {
        this.route.queryParams
            .subscribe(params => this.return = params['return'] || '/');
    }

    // public signin(username, password) {
    //     return of({token: DEMO_TOKEN, user: DEMO_USER})
    //         .pipe(
    //             delay(1000),
    //             map((res: any) => {
    //                 console.log("desde signin")
    //                 this.setUserAndToken(res.token, res, !!res);
    //                 this.signingIn = false;
    //                 return res;
    //             }),
    //             catchError((error) => {
    //                 return throwError(error);
    //             })
    //         );
    //
    // }

    public checkTokenIsValid() {
        return of(this.sessionStorage.getItem(this.APP_USER))
            .pipe(
                map((profile: User) => {
                    this.setUserAndToken(this.getJwtToken(), profile, true);
                    this.signingIn = false;
                    return profile;
                }),
                catchError((error) => {
                    return of(error);
                })
            );
    }

    public signout() {
        this.crudService.post({}, Sesion.CerrarSesinon).subscribe(
            () => {
                this.setUserAndToken(null, null, false);
                this.sessionStorage.clear();
                this.router.navigateByUrl('sessions/signin');
            });
    }

    isLoggedIn(): Boolean {
        return !!this.getJwtToken();
    }

    rutaValida(url):Observable<boolean> {
        return this.crudService.post({
            ruta: url
        }, Permiso.ValidarRutaMedianteRol)
    }

    tieneAccesoMV(url):Observable<boolean> {
        var urlDesencriptado = this.util.desencriptar(url)
        var urlBase64 = btoa(urlDesencriptado)
        return this.crudService.post({
            ruta: urlBase64
        }, Permiso.ValidarAccesoMV)
    }

    getJwtToken() {
        return this.sessionStorage.getItem(this.JWT_TOKEN);
    }

    getUser() {
        return this.sessionStorage.getItem(this.APP_USER);
    }

    setUserAndToken(token: String, user, isAuthenticated: Boolean) {

        this.isAuthenticated = isAuthenticated;
        this.token = token;
        this.user = user;
        this.user$.next(user);
        this.sessionStorage.setItem(this.JWT_TOKEN, token);
        this.sessionStorage.setItem(this.APP_USER, user);
    }
}
