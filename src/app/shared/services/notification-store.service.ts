import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { Router } from "@angular/router";
import {MatSnackBar} from "@angular/material/snack-bar";
import {LocalStoreService} from './local-store.service';
import {ToastrService} from 'ngx-toastr';

@Injectable({
  providedIn: "root"
})
export class NotificationsStoreService {
  private notifications = [];

  private notifications$ = new BehaviorSubject<any[]>([]);

  constructor(private toastr: ToastrService,
              private router: Router,
              private ls: LocalStoreService) {}

  public select$ = () => this.notifications$.asObservable();

  public dispatch(notification) {
    if (notification === "Unauthorized") {
      this.toastr.error("No Authorizado", 'AVISO');
      this.ls.clear();
      this.router.navigate(["/sessions/signin"]);
    } else if( notification !== "" && notification) {
      this.toastr.error(notification, "ERROR");
    }
    this.notifications.push(notification);
    this.notifications$.next([...this.notifications]);
  }
}
