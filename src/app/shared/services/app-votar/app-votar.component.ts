import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, Inject } from '@angular/core';

@Component({
  selector: 'app-confirm',
  template: `<h1 matDialogTitle class="mb-05">{{ data.title }}</h1>
    <div mat-dialog-content class="mb-1">{{ data.message }}</div>
    <div mat-dialog-actions>
      <button
          type="button"
          mat-raised-button
          color="primary"
          (click)="dialogRef.close(true)">OK</button>
    &nbsp;
    <span fxFlex></span>
    <button
        type="button"
        mat-button
        color="warn" 
    (click)="dialogRef.close(false)">
    
      Cancelar</button>
    </div>`,
})
export class AppVotarComponent {
  constructor(
    public dialogRef: MatDialogRef<AppVotarComponent>,
    @Inject(MAT_DIALOG_DATA) public data:any
  ) {}
}
