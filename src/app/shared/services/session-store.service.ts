import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class SessionStoreService {

  private sessionStorage = window.sessionStorage;

  constructor() { }

  public setItem(key, value) {
    value = JSON.stringify(value)
    this.sessionStorage.setItem(key, value)
    return true
  }

  public getItem(key) {
    let value = this.sessionStorage.getItem(key)
    try {
      return JSON.parse(value)
    } catch (e) {
      return null
    }
  }
  
  public clear() {
    this.sessionStorage.clear();
  }
}
