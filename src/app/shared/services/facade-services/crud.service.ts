import {HttpBackend, HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {environment} from '../../../../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};



@Injectable()
export class CrudService {
  public URL_SERVICE: string;

  constructor(private httpClient: HttpClient) {
    this.URL_SERVICE = environment.apiURL;
  }



  post(dto: any, api: string): any | Observable<any> {
    const url = this.URL_SERVICE + api;
    const body = JSON.stringify(dto);
    return this.httpClient.post<any[]>(url, body, httpOptions);
  }


  archivo(multipart, dto: any, api: string): any | Observable<any> {
    const url = this.URL_SERVICE + api;
    const body = JSON.stringify(dto);
    multipart.append('dto', body);
    return this.httpClient.post(url, multipart, {
      responseType: 'blob',
      observe: 'response',
    });
  }

  exportar(dto: any, api: string): any | Observable<any> {
    const params = new HttpParams({
      fromObject: dto,
    });
    // httpOptions.headers.append('Accept', 'application/octet-stream');
    const url = this.URL_SERVICE + api;
    return this.httpClient.get(url, {
      params: params,
      headers: httpOptions.headers,
      responseType: 'blob',
    });
  }

  put(dto: any, api: string): any | Observable<any>{
    const url = this.URL_SERVICE + api;
    const body = JSON.stringify(dto);
    return this.httpClient.put<any[]>(url, body, httpOptions);
  }

  delete(dto: any, api: string): any | Observable<any> {
    const url = `${this.URL_SERVICE}${api}`;
    const params = new HttpParams({
      fromObject: dto,
    });
    return this.httpClient.delete<any[]>(url, {
      params: params,
      headers: httpOptions.headers,
    });
  }


}


@Injectable()
export class CrudServiceNoInterceptor {
  public URL_SERVICE: string;
  private httpClient: HttpClient;
  // constructor(private httpClient: HttpClient) {
  //   this.URL_SERVICE = environment.apiURL;
  // }

  constructor( handler: HttpBackend) {
    this.URL_SERVICE = environment.apiURL;
    this.httpClient = new HttpClient(handler);
  }

  post(dto: any, api: string): any | Observable<any> {
    const url = this.URL_SERVICE + api;
    const body = JSON.stringify(dto);
    return this.httpClient.post<any[]>(url, body, httpOptions);
  }


  archivo(multipart, dto: any, api: string): any | Observable<any> {
    const url = this.URL_SERVICE + api;
    const body = JSON.stringify(dto);
    multipart.append('dto', body);
    return this.httpClient.post(url, multipart, {
      responseType: 'blob',
      observe: 'response',
    });
  }

  exportar(dto: any, api: string): any | Observable<any> {
    const params = new HttpParams({
      fromObject: dto,
    });
    // httpOptions.headers.append('Accept', 'application/octet-stream');
    const url = this.URL_SERVICE + api;
    return this.httpClient.get(url, {
      params: params,
      headers: httpOptions.headers,
      responseType: 'blob',
    });
  }

  put(dto: any, api: string): any | Observable<any>{
    const url = this.URL_SERVICE + api;
    const body = JSON.stringify(dto);
    return this.httpClient.put<any[]>(url, body, httpOptions);
  }

  delete(dto: any, api: string): any | Observable<any> {
    const url = `${this.URL_SERVICE}${api}`;
    const params = new HttpParams({
      fromObject: dto,
    });
    return this.httpClient.delete<any[]>(url, {
      params: params,
      headers: httpOptions.headers,
    });
  }


}
