import { HttpErrorResponse, HttpInterceptor } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';
import {catchError, take} from 'rxjs/operators';
import { NotificationsStoreService } from './notification-store.service';
import {LocalStoreService} from './local-store.service';
import {JwtAuthService} from './auth/jwt-auth.service';
import {SessionStoreService} from './session-store.service';
import { CrudServiceNoInterceptor} from './facade-services/crud.service';
import {Usuario} from '../../views/constantes/apis';

@Injectable({
  providedIn: 'root',
})
export class ErrorInterceptorService implements HttpInterceptor {
  token: string;
  constructor(
    private notificationsStore: NotificationsStoreService,
    private router: Router,
    private ls: LocalStoreService,
    private jwt: JwtAuthService,
    private sessionStorage: SessionStoreService,
    private crudService: CrudServiceNoInterceptor
  ) {
  }

  /**
   *Intercepta las peticiones Http y agrega el token al header para peticiones a la API
   *Si ocurre un error en una petición lo intercepta
   * @param {*} req
   * @param {*} next
   * @returns
   * @memberof ErrorInterceptorService
   */
  public intercept(req, next) {
    this.token = this.jwt.getJwtToken();
    if (this.token) {
      req = req.clone({
        setHeaders: {
          Authorization: 'Bearer ' + this.token,
        },
      });
    }
    return next.handle(req).pipe(catchError(this.handleError.bind(this)));
  }

  /**
   *Método para manejar los errores Http interceptados
   *
   * @private
   * @param {*} err
   * @returns
   * @memberof ErrorInterceptorService
   */
  private handleError(err) {
    const unauthorized_code = 401;
    const error_code = 500;
    let userMessage;
    if (err instanceof HttpErrorResponse) {
      if (err.status === unauthorized_code) {
        userMessage = 'Sesión Caducada';
        this.crudService.post({dt1: this.jwt.getJwtToken()},Usuario.VaciarSesionUsuario).subscribe(take(1));
        setTimeout(()=>
        {
          this.ls.clear();
          this.sessionStorage.clear();
          this.router.navigate(['/sessions/signin']);
        });
      } else if (err.status === error_code) {
        userMessage = 'Error: Conexión de Datos';
        this.ls.clear();
        this.sessionStorage.clear();
        this.router.navigate(['/sessions/signin']);
      } else if (err.status === 504) {
        userMessage = 'Error: Conexión de Datos';
        this.ls.clear();
        this.sessionStorage.clear();
        this.router.navigate(['/sessions/signin']);
      }
    }
    if (!userMessage) {
      if (err.error.errorExceptionMessage) {
        userMessage = err.error.errorExceptionMessage.descripcion;
      } else if (err.error.informationMessage) {
        userMessage = err.error.informationMessage.descripcion;
      } else {
        userMessage = "Error de Conexión"
      }
    }
    if(userMessage == 'Unauthorized')
    {
        this.ls.clear();
        this.sessionStorage.clear();
        this.router.navigate(['/sessions/signin']);
    }
    this.notificationsStore.dispatch(
      userMessage
    );
    return throwError(err);
  }
}
