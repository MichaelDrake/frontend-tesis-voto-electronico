import {Injectable} from '@angular/core';
import {MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition} from '@angular/material/snack-bar';
import {ToastrService} from 'ngx-toastr';
import {ApiResponseMessageModel} from '../../models/voto-electronico/api-response-message.model';
import {SecurityEncripted} from '../../../../config';
import * as CryptoJS from 'crypto-js'

@Injectable()
export class UtilService {

    constructor(
        private toastr: ToastrService
    ) {
    }

    obtenerListaMensaje(apiResponseMessage: ApiResponseMessageModel, first?: boolean): any | any[] {
        if (!apiResponseMessage) {
            return first ? {} : [];
        }
        if (!apiResponseMessage.responseList) {
            return first ? {} : [];
        }
        return first ? apiResponseMessage.responseList[0] : apiResponseMessage.responseList;
    }

    existenErrores(_apiResponseMessage: ApiResponseMessageModel) {
        var error = false;

        if (_apiResponseMessage.errorExceptionMessage != null) {
            this.toastr.error(_apiResponseMessage.errorExceptionMessage.descripcion, _apiResponseMessage.errorExceptionMessage.tipoMensajeId);
            error = true;
        }
        return error;
    }


    mostrarMensaje(_apiResponseMessage: ApiResponseMessageModel) {
        if (_apiResponseMessage.informationMessage != null) {
            if (_apiResponseMessage.informationMessage.tipoMensajeId == 'INFO') {

                this.toastr.success(
                    _apiResponseMessage.informationMessage.descripcion,
                    _apiResponseMessage.informationMessage.tipoMensajeId);

            } else if (_apiResponseMessage.informationMessage.tipoMensajeId == 'AVISO') {
                this.toastr.warning(
                    _apiResponseMessage.informationMessage.descripcion,
                    _apiResponseMessage.informationMessage.tipoMensajeId);
            }
        }
    }

    //Funcion para encriptar id de rutas
    encriptar(valor: string) {
        const textoEncriptado = CryptoJS.AES.encrypt(JSON.stringify(valor), SecurityEncripted.KEY).toString();
        return textoEncriptado;
    }

    //Funcion para desencriptar id de rutas
    desencriptar(textoEncriptado: string) {
        var bytes = CryptoJS.AES.decrypt(textoEncriptado, SecurityEncripted.KEY);
        var decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
        return decryptedData;
    }

    /*
    * Date 1/12/20
    * Funcion para generar (h), pass del voto.
    * */
    generarMascara(votoCifradoAes) {
        var instante1 = Date.now();
        var random = this.getRandomArbitrary(0, 9999999999);
        var instante2 = Date.now() + random;
        var mensajeAEncriptar = instante1.toString().concat(votoCifradoAes,instante2.toString())
        var hash = CryptoJS.SHA3(mensajeAEncriptar, { outputLength: 512 });
        return hash
    }

    // Retorna un número aleatorio entre min (incluido) y max (excluido)
    getRandomArbitrary(min, max) {
        return Math.round(Math.random() * (max - min) + min);
    }

    encriptarVotoConAes(voto, key,iv) {
        var votoString = JSON.stringify(voto);
        var keyUTF8 = CryptoJS.enc.Utf8.parse(key);
        var ivUTF8 = CryptoJS.enc.Utf8.parse(iv);
        var cifradoAES = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(votoString), keyUTF8, {
            //keySize: 128/8,
            iv: ivUTF8,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });

        return cifradoAES;
    }

    generarKeyAes(){
        return this.keyGen(32)
    }

    generarVectorInicializacionAes(){
        return this.keyGen(16)
    }

    keyGen(keyLength) {
        var characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        var pass = "", i;
        for ( i=0; i < keyLength; i++){
                pass += characters.charAt(Math.floor(Math.random()*characters.length));
        }
        return pass;
    }
    soloNumeros(event: any) {
        const pattern = /[0-9]/;
        const inputChar = String.fromCharCode(event.charCode);
        if (!pattern.test(inputChar)) {
            // carácter inválido, prevenir entrada
            event.preventDefault();
        }
    }

}

