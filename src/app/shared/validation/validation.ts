import {AbstractControl} from '@angular/forms';
import { UtilService } from '../services/util/util.service';
export class CustomValidator {
    constructor(private util: UtilService) {
    }



    /**
     *Validación de Passwords iguales
     *
     * @static
     * @param {*} group
     * @returns {*}
     * @memberof CustomValidator
     */
    static matchPassword(group): any {
        if (!group) return null;
        const password = group.controls.password;
        const confirm = group.controls.confirmPassword;
        if (password.pristine || confirm.pristine) {
            return null;
        }
        group.markAsTouched();
        if (password.value === confirm.value) {
            return null;
        }
        return {
            invalidPassword: true,
        };
    }





    static safePassword(
        control: AbstractControl
    ): { [key: string]: boolean } | null {
        const regex = new RegExp(
            '^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*["@!#$%&/()=?¡¿Q´*[{}_+*.,;:|\\]-])[A-Za-z\\d"@!#$%&/()=?¡¿Q´*[{}_+*.,;:|\\]-]{7,10}'
        );
        if (!regex.test(control.value)) {
            return {safePassword: true};
        }
        return null;
    }


}
