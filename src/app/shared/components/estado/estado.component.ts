import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl} from '@angular/forms';

@Component({
    selector: 'app-estado',
    templateUrl: './estado.component.html',
    styleUrls: ['./estado.component.less']
})
export class EstadoComponent implements OnInit {
    @Input() cntrl:FormControl;
    @Output() cntrlChange: EventEmitter<FormControl> = new EventEmitter();
    defaultSelected = 'ACTIVO'
    constructor() {
    }

    ngOnInit(): void {
    }

}
