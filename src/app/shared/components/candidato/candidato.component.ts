import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';


@Component({
  selector: 'app-candidato',
  templateUrl: './candidato.component.html',
  styles: [
  ]
})
export class CandidatoComponent implements OnInit {

  @Input() candidato;
  @Input() procesoSelected;
  @Output() deleteItem = new EventEmitter<any>();
  esAlterno:boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  verAlterno()
  {
    this.esAlterno = true;
  }
  verPrincipal()
  {
    this.esAlterno = false;
  }


}
