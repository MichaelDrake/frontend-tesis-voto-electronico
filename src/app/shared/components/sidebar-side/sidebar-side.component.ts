import { Component, OnInit, OnDestroy, AfterViewInit } from "@angular/core";
import { NavigationService } from "../../../shared/services/navigation.service";
import { ThemeService } from "../../services/theme.service";
import {Subject, Subscription} from 'rxjs';
import { ILayoutConf, LayoutService } from "app/shared/services/layout.service";
import { JwtAuthService } from "app/shared/services/auth/jwt-auth.service";
import {UtilService} from '../../services/util/util.service';
import {SessionStoreService} from '../../services/session-store.service';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: "app-sidebar-side",
  templateUrl: "./sidebar-side.component.html"
})
export class SidebarSideComponent implements OnInit, OnDestroy, AfterViewInit {
  public menuItems: any[] = [];
  public hasIconTypeMenuItem: boolean;
  public iconTypeMenuTitle: string;
  public layoutConf: ILayoutConf;
  private unsuscribe$ = new Subject<void>(); //marca como completado el observable
  constructor(
    private navService: NavigationService,
    public themeService: ThemeService,
    private layout: LayoutService,
    public jwtAuth: JwtAuthService,
    private util: UtilService,
    private sesionService: SessionStoreService
  ) {}

  ngOnInit() {
    this.iconTypeMenuTitle = this.navService.iconTypeMenuTitle;
    var menu = this.sesionService.getItem("menuItems");
    if(!menu || menu.length == 0 )
    {
      this.navService.cargarMenu().pipe(
          takeUntil(this.unsuscribe$) // previene uso de memoria infinita
      ).subscribe(
          (res)=> {
            var listaRespuesta = this.util.obtenerListaMensaje(res);
            this.menuItems = listaRespuesta;
            this.sesionService.setItem("menuItems",listaRespuesta)
          }
      );
    }
    else
    {
      this.menuItems = menu;
    }
    this.layoutConf = this.layout.layoutConf;
  }

  ngAfterViewInit() {}
  ngOnDestroy() {
    this.unsuscribe$.next(); // enviar el siguiente dato
    this.unsuscribe$.complete(); // marca como completado al observable
  }
  toggleCollapse() {
    if (
      this.layoutConf.sidebarCompactToggle
    ) {
        this.layout.publishLayoutChange({
        sidebarCompactToggle: false
      });
    } else {
        this.layout.publishLayoutChange({
            // sidebarStyle: "compact",
            sidebarCompactToggle: true
          });
    }
  }
}
