import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {UtilService} from 'app/shared/services/util/util.service';
import {Subject} from 'rxjs';
import {map, startWith, switchMap} from 'rxjs/operators';
import {CrudService} from '../../services/facade-services/crud.service';

@Component({
    selector: 'app-input-autocomplete',
    templateUrl: './input-autocomplete.component.html',
    styleUrls: ['./input-autocomplete.component.scss']
})

export class InputAutocompleteComponent implements OnInit {
    public entidades: any[] = [];
    filteredOptions: any;
    itemForm: FormGroup;
    timer: any;
    loader: boolean = false;
    private keyObs$: Subject<any>;

    @Input() titulo: string;
    @Input() cntrl: FormControl = new FormControl();
    @Output() cntrlChange = new EventEmitter<FormControl>();
    @Input() api: string;
    @Input() parametros: any;
    @Input() appearance: string;
    @Input() mat_label: string;
    @Input() readOnly: boolean;
    @Input() errorCode;
    @Input() mensajeError;
    @Input() valoresOmitir: any[];

    constructor(
        private crudService: CrudService,
        public utilService: UtilService
    ) {
        this.keyObs$ = new Subject();
    }

    ngOnInit() {
        this.autocompleteEscucha();
        this.escucharInputEntidades();
    }

    /**
     *
     * metodo que maneja el evento de tipeo en el teclado
     * @param event
     */
    handleKeyboardEvent(event) {

        if (event.code.includes('Arrow')) {
            return;
        }
        if (this.timer) {
            clearTimeout(this.timer);
        }

        let key = event.target.value;
        let longitudKey = key.length;
        if (longitudKey > 0 && longitudKey < 17) {
            this.timer = setTimeout(() => {
                this.keyObs$.next(key.trim())
            }, 500)
        }
    }


    /**
     * Escucha el evento on paste para realizar la consulta de datos
     * @param event
     */
    onPaste(event: ClipboardEvent) {
        // @ts-ignore
        let clipboardData = event.clipboardData || window.clipboardData;
        let pastedText = clipboardData.getData('text');
        this.keyObs$.next(pastedText);
    }

    /**
     * Metodo Para escuchar el cambio de las opciones filtradas segun el texto del imput
     */
    autocompleteEscucha() {
        this.filteredOptions = this.cntrl.valueChanges.pipe(
            startWith(''),
            map((value) => this._filter(value))
        );
    }

    /**
     * Metodo para filtrar las entidades según el parametro value
     * @param value -  parametro por el cual se va a filtrar
     * @private
     */
    private _filter(value: any): string[] {
        if (value == null) {
            return;
        }
        const valores = value.dt1 ? value.dt1.split(/\s+/) : value.split(/\s+/);
        return this.entidades.filter((option) =>
            this.ContainsAny(
                option.dt1.toLowerCase(),
                valores.length > 0 ? valores : value
            )
        );
    }

    /**
     * Metodo para setear que parametro del objeto se muestra en el Autocomplete
     * @param option
     */
    displayFn(option?): string | undefined {
        return option ? option.dt1 : undefined;
    }


    /**
     * Observable que escucha el tipeo del teclado
     */
    escucharInputEntidades() {
        this.keyObs$
            .pipe(
                switchMap((value) => {
                    this.loader = true;
                    const dto1 = {
                        parametroBusqueda1: value,
                    };
                    const dto = this.parametros
                        ? Object.assign(dto1, this.parametros) : dto1;
                    return this.crudService.post(dto, this.api);
                })
            )
            .subscribe((result: any[]) => {
                    if (this.valoresOmitir) {
                        result = result.filter(x => !this.valoresOmitir.some(y => y === x.dt1));
                    }

                    this.entidades = result;
                    this.loader = false;
                    this.autocompleteEscucha();
                },
                () => this.loader = false
            );
    }

    ContainsAny(cadenaComparar: string, palabrasComparando: string[]): boolean {
        var match = false;
        palabrasComparando.forEach((c) => {
            if (cadenaComparar.toLowerCase().includes(c.toLowerCase())) {
                match = true;
            } else {
                match = false;
            }
        });
        return match;
    }
}
