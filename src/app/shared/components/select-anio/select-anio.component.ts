import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-select-anio',
  templateUrl: './select-anio.component.html',
  styleUrls: ['./select-anio.component.scss']
})
export class SelectAnioComponent implements OnInit {
  @Input() cntrl:FormControl;
  @Output() cntrlChange: EventEmitter<FormControl> = new EventEmitter();
  listaAnios = [];

  constructor() { }

  ngOnInit(): void {
    this.iniciarListaAnios();
  }

  iniciarListaAnios(){
    var fechaActual = new Date();
    var anioActual = fechaActual.getFullYear()
    for(var i=10; i>=0; i--){
      this.listaAnios.push({
        id:anioActual,
        value: anioActual
      })
      anioActual--;
    }
  }


}
