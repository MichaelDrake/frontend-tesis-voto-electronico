import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {JwtAuthService} from '../services/auth/jwt-auth.service';
import {CrudService} from '../services/facade-services/crud.service';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ValidarAccesoMvGuard implements CanActivate {

  constructor(private jwtAuth: JwtAuthService,
              private crudService: CrudService,
              private router: Router,

  ) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

      // console.log(next)
      // console.log(next.params.procesoElectoralId)
      // console.log(state)
      // return true;
    return this.jwtAuth.tieneAccesoMV(next.params.procesoElectoralId).pipe(map(e => {
          if (e) {
            return true;
          } else {
            this.router.navigate(['/others/blank']);
            return false;
          }
        }),
    )
  }
  
}
