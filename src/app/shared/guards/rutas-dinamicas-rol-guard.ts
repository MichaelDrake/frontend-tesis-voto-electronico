import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {CrudService} from '../services/facade-services/crud.service';
import {Permiso} from '../../views/constantes/apis';
import {ApiResponseMessageModel} from '../models/voto-electronico/api-response-message.model';
import {JwtAuthService} from '../services/auth/jwt-auth.service';
import 'rxjs/add/observable/of';
import {map} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})

export class RutasDinamicasPorRolGuardGuard implements CanActivate {

    constructor(private jwtAuth: JwtAuthService,
                private crudService: CrudService,
                private router: Router,
    ) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> |
        Promise<boolean | UrlTree> | boolean | UrlTree {
        const a = route?.parent?.routeConfig?.path
        const b = route?.routeConfig?.path
        const ruta = `/${a}/${b}`
        return this.jwtAuth.rutaValida(ruta).pipe(map(e => {
                if (e) {
                    return true;
                } else {
                    this.router.navigate(['/others/blank']);
                    return false;
                }
            }),
        )

    }

}
