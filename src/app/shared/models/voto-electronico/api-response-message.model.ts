export interface ApiResponseMessageModel {
    errorExceptionMessage?: Mensaje;
    informationMessage?: Mensaje;
    responseList?;
}

export  interface Mensaje {
    codigoMensaje? : string;
    descripcion? : string;
    tipoMensajeId?: string;
    titulo? : string;
}

export  interface Candidato {
    candidatoId? : number;
    listaId? : number;
    escanioId?: number;
    nombreCandidato?: string;
    nombreEscanio?: string;
    nombreLista?: string;
}
