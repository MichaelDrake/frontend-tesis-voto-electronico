export const Login = {
    IniciarSesion:"Lgn/VELgnUsr"
};

export const Sesion = {
    CerrarSesinon : "Ssn/VELlgOut",
    ValidarToken : "Ssn/VExTkn",
    CambiarClave : "Ssn/VECmbClv",
    SolicitarCambioClave: "Ssn/SlcCmCv"
};

export const Escanio = {
    CrearEscanio :"Scn/VELCrEsc",
    ObtenerEscanios: "Scn/VELSlEsc",
    ActualizarEscanio: "Scn/VELUpEsc",
    EliminarEscanio: "Scn/VELDlEsc",
    ObtenerEscaniosDisponiblesMedianteListaId: "Scn/VELSlEscLisId",
    ObtenerEscaniosDisponiblesMedianteProcesoElectoralId: "Scn/VELSlEscPe",
    ObtenerEscaniosPorEleccion: "Scn/VESlEscEl",
    ReactivarEscanios: "Scn/VERCESc"
};
export const Padron = {
    ImportarPadron : "MptrPdr/VmMptPdr",
    ObtenerPadronesNombre: "PdrVtc/VESlPdr",
    ValidarVotoUsuario: "PdrVtc/AutUsrPdr",
    AñadirEmpadronado: "PdrVtc/VeCrPel",
    EliminarRegistro:"PdrVtc/eliminar-padron-votacion",
}


export const Eleccion = {
    CrearEleccion :"Elc/VELCrElc",
    ObtenerEleccionById: "Elc/VELObtElcById",
    ActualizarEleccion: "Elc/VELUpElc",
    EliminarEleccion: "Elc/VELDlElc",
    ObtenerEleccionByParametros: "Elc/VELObtElcByPrms",
    ReactivarEleccion: "Elc/VERcElcc"
}

export const TipoEleccion = {
    ObtenerEleccionByParametros: "TpElc/VELObtTpElcByPrms",
}

export const ProcesoElectoral = {
    ObtenerProcesosElectoralesByParametros: "Pelc/VELObtPelcByPrms",
    CrearProcesoElectoral:"Pelc/VELCrPelc",
    ActualizarProcesoElectoral:"Pelc/VELUpPelc",
    EliminarProcesoElectoral:"Pelc/VELDlPelc",
    ObtenerProcesosVigentesByToken: "Pelc/VELObtPelcVig",
    ObtenerProcesoElectoralById: "Pelc/VELObtPelcById",
    ObtenerProcesosActivos: "Pelc/VESLPrcCtv",
    ObtenerProcesosFinalizadosByParametros: "Pelc/VELPelFnlz",

}

export const Lista = {
    ObtenerListasPorParametros: "Lis/VELObtLisByPrms",
    CrearLista:"Lis/VELCrLis",
    ActualizarLista:"Lis/VELUpLis",
    EliminarLista:"Lis/VELDelLis",
    ObtenerListasPorProcesoElectoralId:"Lis/VELObtLisByProcEl",
    ObtenerListasCandidatosByProcesoId:"Lis/VELObtLisCandProcEl",
}


export const Candidato = {
    ObtenerCandidatosPorParametros: "Cnd/VELObtCndByParams",
    CrearCandidato:"Cnd/VELCrCnd",
    ActualizarCandidato:"Cnd/VELUpCnd",
    EliminarCandidato:"Cnd/VELDlCnd",
    ObtenerCandidatoMedianteListaId: "Cnd/VELObtCndByLisId",
}


export const Permiso = {
    ValidarRutaMedianteRol: "Prm/ValRtaRl",
    CrearPermiso: "Prm/VECrPrms",
    ValidarAccesoMV: "Prm/ValAccMV",
}
export const MenuUsuario = {
    ObtenerMenuUsuario: "Rcrs/VEslMnUsr"
}

export const MesaVoto = {
    EmitirVoto: "MV/EmtVt",
    VerificarVoto: "MV/ObtVtMas"
}


export const Votos = {
    ObtenerResumenVotos: "Vt/VeRsmPrc"
}
export const Roles = {
    ObtenerRolesNombre: "Roles/VESlRlsCb",
    ObtenerRolesNombreNoAdmin: "Roles/VESlRlsCbNoAd",
    ObtenerRolesNombreNoAdminRuta:"Roles/VESlRlsCbNoAdRt",
    ObtenerListaRolesActivos : "Roles/VESlTdsRls"
}
export const Modulos = {
    ObtenerModulosActivos: "Rcrs/VESLMdlsS",
    ObtenerRutasActivas: "Rcrs/VESLMdlsSRt"

}
export const Persona = {
    ObtenerPersonasMedianteParametrosBusqueda: "persona/obtPerPrms",
    CrearPersona: "persona/crear-persona",
    ActualizarPersona: "persona/actualizar-persona",
    EliminarPersona: "persona/IncPer",
    ReactivarPersoan : "persona/ActPer",
}


export const Usuario = {
    ObtenerListaUsuariosMedianteParametrosBusqueda: "usuario/obtUsrPrms",
    CrearUsuario: "usuario/VeCrUsrWb",
    CambiarEstadoUsuario: "usuario/VeIncUsr",
    ObtenerUsuarioMedianteToken: "usuario/VeObUsrTkn",
    ActualizarUsuarioWeb: "usuario/VeUpUsrWb",
    ReenviarEmailActivacion: "usuario/VERmvMl",
    VaciarSesionUsuario:"usuario/VErcLsT",
}

export const UsuarioRol = {
    ActualizarUsuarioRol: "usrrl/VEUpUsrRl"
}

export const Cargo = {
    ObtenerCargoToken:"CtCrg/VESLCrgTkn",
    ObtenerListaCargosActivos : "CtCrg/VeSlCrgAct",
}
