export const Auditoria = {
    ACTIVO: 'ACTIVO',
    INACTIVO: 'INACTIVO',
}

export const BadgeEstados = {
    EnProceso: {
        color: '#2ECC71',
        text: 'En proceso',
    },
    Finalizado: {
        color: '#CC0000',
        text:
            'Finalizado',
    }
    ,
    NoIniciado: {
        color: '#F1C40F',
        text: 'No Iniciado',
    }
}

export const PaletaColoresPerzonalizados = {
    color1: '#009688',
    color2: '#0B2136',
    color3: '#2ECC71',
    //color3: 'green-A700'
}

//hora en formato 24h (0 a 23).
export const horas = {
    horaInicio: 7,
    horaFin: 17,
}


export const VotoEstados = {
    Blanco: {
        color: '#515A5A',
        estado: 'VOTO EN BLANCO',
        mensaje: 'Aun no ha seleccionado opciones'
    },
    Nulo: {
        color: '#D98880',
        estado: 'VOTO NULO',
        mensajeOpcionesExcedidas: 'Ha seleccionado mas de las opciones permitidas',
        mensajeListasExcedidas: 'Ha seleccionado mas de una lista',
        mensajeOpcionesFaltantes: 'Faltan escanios a escoger',
        mensajeEscaniosRepetidos: 'Existe mas de un voto para un mismo escanio'
    }
    ,
    Valido: {
        color: '#2ECC71',
        estado: 'VOTO VALIDO',
        mensaje: ''
    }
}


export const OpcionesBusquedaProcesos = [{
    id: 'ANIO',
    value: 'AÑO'
},
    {
        id: 'NOMBRE',
        value: 'NOMBRE PROCESO ELECTORAL'
    },
    {
        id: 'ESTADO',
        value: 'ESTADO'
    }
]

export const OpcionesBusquedaProcesosFinalizados = [{
    id: 'ANIO',
    value: 'AÑO'
},
    {
        id: 'NOMBRE',
        value: 'NOMBRE PROCESO ELECTORAL'
    },
]

export const OpcionesBusquedaModuloPersona = [{
    id: 'IDENTIFICACION',
    value: 'IDENTIFICACIÓN'
},
    {
        id: 'NOMBRE',
        value: 'NOMBRE'
    },
    {
        id: 'EMAIL',
        value: 'CORREO'
    },
    {
        id: 'ESTADO',
        value: 'ESTADO'
    }
]

export const OpcionesBusquedaModuloUsuario = [
    {
        id: 'USUARIO',
        value: 'USUARIO'
    },
    {
        id: 'ESTADO',
        value: 'ESTADO'
    }
]
