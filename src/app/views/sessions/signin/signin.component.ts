import { Component, OnInit, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatButton } from '@angular/material/button';
import { MatProgressBar } from '@angular/material/progress-bar';
import {Validators, FormGroup, FormControl, FormBuilder} from '@angular/forms';
import { Subject } from 'rxjs';
import { AppLoaderService } from '../../../shared/services/app-loader/app-loader.service';
import { JwtAuthService } from '../../../shared/services/auth/jwt-auth.service';
import {CrudServiceNoInterceptor} from '../../../shared/services/facade-services/crud.service';
import {Login, Sesion} from '../../constantes/apis';
import {AppConfirmService} from '../../../shared/services/app-confirm/app-confirm.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {UtilService} from '../../../shared/services/util/util.service';
import {ToastrService} from 'ngx-toastr';
import {User} from '../../../shared/models/user.model';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['../sessions.component.less']
})

export class SigninComponent implements OnInit, OnDestroy {
  @ViewChild(MatProgressBar) progressBar: MatProgressBar;
  @ViewChild(MatButton) submitButton: MatButton;

  signinForm: FormGroup;
  errorMsg = '';

  private unsuscribe$ = new Subject<void>(); //marca como completado el observable

  constructor(
    private egretLoader: AppLoaderService,
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private crudService: CrudServiceNoInterceptor,
    private confirmService: AppConfirmService,
    private snack: MatSnackBar,
    private util: UtilService,
    private jwt: JwtAuthService,
    private toastr: ToastrService
  ) {
    if(this.jwt.isLoggedIn())
    {
      this.router.navigate(['others','blank'])
    }
  }

  ngOnInit() {


    this.signinForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }


  ngOnDestroy() {
    this.unsuscribe$.next();
    this.unsuscribe$.complete();
  }

  signin() {
    const signinData = this.signinForm.value

    this.submitButton.disabled = true;
    this.progressBar.mode = 'indeterminate';


    const loginRequest = this.crudService.post({dt1: signinData.username, dt2: btoa(signinData.password)}, Login.IniciarSesion);

    loginRequest.pipe(
        takeUntil(this.unsuscribe$)
    ).subscribe(response => {
      this.successLogin(response)
    }, error => {
      this.submitButton.disabled = false;
      this.progressBar.mode = 'determinate';
      this.errorMsg = error.message;

      const errorMensaje = error.error?
          error.error.errorExceptionMessage?
              error.error.errorExceptionMessage.descripcion:
              error.error.informationMessage?
                  error.error.informationMessage.descripcion :
                  "Error de conexión":"Error de conexión";

      if (errorMensaje === 'Inició Sesión en otro lugar') {
        this.confirmService
            .confirm({title: 'Usted tiene sesiones activas', message: '¿Desea cerrarlas?'})
            .subscribe((result) => {
              if (result) {
                  this.submitButton.disabled = true;
                  this.progressBar.mode = 'indeterminate';
                  
                const dto = {Dt1: signinData.username};
                this.crudService.post(dto, Sesion.CerrarSesinon )
                    .pipe(
                        takeUntil(this.unsuscribe$)
                    ).subscribe(() => {
                      loginRequest.subscribe((data) => this.successLogin(data));
                    });
              }
            });
      } else {
        this.toastr.error(errorMensaje,"ERROR");
      }

    })
  }
  successLogin(data: any) {
    const dto = this.util.obtenerListaMensaje(data, true);
    if (dto.dt8) {
      var userEvote: User = {
        displayName: dto.dt2,
        imagen: dto.dt3,

      }

      this.jwt.setUserAndToken(dto.dt8,userEvote,true)
      setTimeout(()=>{this.router.navigate(['/others/blank']);})
    }
  }




}
