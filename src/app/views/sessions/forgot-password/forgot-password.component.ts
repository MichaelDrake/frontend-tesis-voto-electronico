import {Component, OnInit, ViewChild} from '@angular/core';
import {MatProgressBar} from '@angular/material/progress-bar';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subject} from 'rxjs';
import {CrudService} from '../../../shared/services/facade-services/crud.service';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';
import {UtilService} from '../../../shared/services/util/util.service';
import {Sesion} from '../../constantes/apis';
import {takeUntil} from 'rxjs/operators';
import {JwtAuthService} from '../../../shared/services/auth/jwt-auth.service';

@Component({
    selector: 'app-forgot-password',
    templateUrl: './forgot-password.component.html',
    styleUrls: ['../sessions.component.less']
})
export class ForgotPasswordComponent implements OnInit {
    @ViewChild(MatProgressBar, {static: false}) progressBar: MatProgressBar;
    recuperarForm: FormGroup;
    private unsuscribe$ = new Subject<void>(); //marca como completado el observable
    constructor(
        private crudService: CrudService,
        private toastr: ToastrService,
        private router: ActivatedRoute,
        private route: Router,
        public util: UtilService,
        private fb: FormBuilder,
        private jwt : JwtAuthService,
    ) {
    }

    ngOnInit() {
        if(this.jwt.isLoggedIn())
        {
            this.route.navigate(['others','blank'])
        }
        this.buildItemForm();
    }


    buildItemForm() {
        this.recuperarForm = this.fb.group({
            email: [
                '',
                [
                    Validators.required,
                ],
            ]
        });
    }

    ngOnDestroy() {
        this.unsuscribe$.next(); // enviar el siguiente dato
        this.unsuscribe$.complete(); // marca como completado al observable
    }

    /**
     *Envío petición cambio contraseña
     *
     * @memberof CambioClaveComponent
     */
    submitCambio() {
        this.progressBar.mode = 'indeterminate';
        const data = this.recuperarForm.value;
        this.solicitarCambiarClave(data.email);
    }

    /**
     *Método para cambio de contraseña
     *
     * @param {string} password
     * @param {string} [token]
     * @param {string} [usuario]
     * @memberof CambioClaveComponent
     */
    solicitarCambiarClave(email: string) {
        const dto = {
            dt1: email
        };
        this.crudService
            .post(dto, Sesion.SolicitarCambioClave)
            .pipe(
                takeUntil(this.unsuscribe$) // previene uso de memoria infinita
            )
            .subscribe((result) => {
                    this.util.mostrarMensaje(result);
                    this.progressBar.mode = 'determinate';
                    this.route.navigate(['/sessions/signin']);
                },
                () => {
                    this.progressBar.mode = 'determinate';
                    this.route.navigate(['/sessions/signin']);
                });
    }
}
