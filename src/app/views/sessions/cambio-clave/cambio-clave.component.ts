import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subject} from 'rxjs';
import {CrudService} from '../../../shared/services/facade-services/crud.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ActivatedRoute, Router} from '@angular/router';
import {CustomValidator} from '../../../shared/validation/validation';
import {Login, Sesion} from '../../constantes/apis';
import {takeUntil} from 'rxjs/operators';
import {MatProgressBar} from '@angular/material/progress-bar';
import {ToastrService} from 'ngx-toastr';
import {UtilService} from '../../../shared/services/util/util.service';
import {JwtAuthService} from '../../../shared/services/auth/jwt-auth.service';

@Component({
    selector: 'app-cambio-clave',
    templateUrl: './cambio-clave.component.html',
    styleUrls: ['../sessions.component.less']
})
export class CambioClaveComponent implements OnInit, OnDestroy {
    @ViewChild(MatProgressBar, {static: false}) progressBar: MatProgressBar;
    recuperarForm: FormGroup;
    token: string;
    password;
    confirmPassword;
    verified:boolean = false;
    private unsuscribe$ = new Subject<void>(); //marca como completado el observable
    constructor(
        private crudService: CrudService,
        private toastr: ToastrService,
        private router: ActivatedRoute,
        private route: Router,
        public util: UtilService,
        private fb: FormBuilder,
        private jwt : JwtAuthService,
    ) {
        if(this.jwt.isLoggedIn())
        {
            this.route.navigate(['others','blank'])
        }
    }

    ngOnInit() {

        this.verificarExisteParametroToken();
        this.buildItemForm();
        this.escucharClaves();
    }


    buildItemForm() {
        this.recuperarForm = this.fb.group({
            password: [
                '',
                [
                    Validators.required,
                    Validators.minLength(8),
                    CustomValidator.safePassword,
                ],
            ],
            confirmPassword: ['', Validators.required],
        });
    }

    escucharClaves() {
        this.recuperarForm.controls['confirmPassword'].valueChanges.subscribe(
            (v) => {
                if (v === this.confirmPassword) {
                    return;
                }
                this.confirmPassword = v;
                const validation = CustomValidator.matchPassword(this.recuperarForm);
                if (validation) {
                    //si existe un mensaje de error
                    this.recuperarForm.controls['confirmPassword'].setErrors(validation);
                }
                this.recuperarForm.controls['password'].updateValueAndValidity();
            }
        );
        this.recuperarForm.controls['password'].valueChanges.subscribe((v) => {
            if (v === this.password) {
                return;
            }
            this.password = v;
            const validation = CustomValidator.matchPassword(this.recuperarForm);
            if (validation) {
                //si existe un mensaje de error
                this.recuperarForm.controls['password'].setErrors(validation);
            }
            this.recuperarForm.controls['confirmPassword'].updateValueAndValidity();
        });
    }

    ngOnDestroy() {
        this.unsuscribe$.next(); // enviar el siguiente dato
        this.unsuscribe$.complete(); // marca como completado al observable
    }

    /**
     *Envío petición cambio contraseña
     *
     * @memberof CambioClaveComponent
     */
    submitCambio() {
        this.progressBar.mode = 'indeterminate';
        const data = this.recuperarForm.value;
        this.cambiarClave(data.password, this.token);
    }

    /**
     *Método para cambio de contraseña
     *
     * @param {string} password
     * @param {string} [token]
     * @param {string} [usuario]
     * @memberof CambioClaveComponent
     */
    cambiarClave(password: string, token?: string) {
        const dto = {
            dt1: btoa(password),
            dt20: token,
        };
        this.crudService
            .post(dto, Sesion.CambiarClave)
            .pipe(
                takeUntil(this.unsuscribe$) // previene uso de memoria infinita
            )
            .subscribe((result) => {
              var respuesta = this.util.obtenerListaMensaje(result, true);
                    const respuestaCambioClave = respuesta.bdt1;
                    if (respuestaCambioClave) {
                        this.util.mostrarMensaje(result);
                        this.route.navigate(['/sessions/signin']);
                    } else {
                      this.util.mostrarMensaje(result);
                      this.recuperarForm.reset();
                        this.progressBar.mode = 'determinate';
                    }
                },
                () => {
              this.progressBar.mode = "determinate";
                  this.route.navigate(['/sessions/signin']);
                });
    }



    verificarExisteParametroToken() {
        this.router.queryParams.subscribe((params) => {
            this.token = params['tkn'];
            if(!this.token)
            {
              this.toastr.error("No Autorizado");
              this.route.navigate(['/sessions/signin']);
            }
            else {
              this.crudService.post({dt20: this.token},Sesion.ValidarToken)
                  .pipe(takeUntil(this.unsuscribe$))
                  .subscribe(
                      (res)=>{
                        var respuesta = this.util.obtenerListaMensaje(res, true);
                        if(respuesta.bdt1)
                        {
                          this.verified = true;
                        }
                        else {
                          this.toastr.error("No Autorizado");
                          this.route.navigate(['/sessions/signin']);
                        }
                      },
                      ()=>
                      {
                        this.route.navigate(['/sessions/signin']);
                      }
                  )

            }
        });
    }
}
