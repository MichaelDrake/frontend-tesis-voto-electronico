import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CrudService} from '../../../../shared/services/facade-services/crud.service';
import {forkJoin, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {ProcesoElectoral, Votos} from '../../../constantes/apis';
import {UtilService} from '../../../../shared/services/util/util.service';
import {ActivatedRoute, Router} from '@angular/router';
import {
    Auditoria,
    OpcionesBusquedaProcesosFinalizados
} from '../../../constantes/variables-globales';
import {ApiResponseMessageModel} from '../../../../shared/models/voto-electronico/api-response-message.model';
import {AppLoaderService} from '../../../../shared/services/app-loader/app-loader.service';

@Component({
    selector: 'app-resultados',
    templateUrl: './resultados.component.html',
    styleUrls: ['./resultados.component.less']
})
export class ResultadosComponent implements OnInit, OnDestroy{
    opcionesDeBusqueda = [];
    itemForm: FormGroup;
    lsProcesos: any[];
    propiedades: any
    proceso: any;
    detalleProceso: any;
    eleccionUnipersonal = 'Unipersonal';
    eleccionPluripersonal = 'Pluripersonal';
    parametroDeBusquedaSeleccionado;
    divisibles:any[] = [];
    listaGanadores:any[] = [];
    displayedColumns: string[] = ['cargos','consignados', 'ponderados'];
    listaGanadoresNoCumplenMinimo:any[] = [];
    private unsuscribe$ = new Subject<void>(); //marca como completado el observable
    constructor(private fb: FormBuilder,
                private crudService: CrudService,
                public util: UtilService,
                private router: Router,
                private activatedRoute: ActivatedRoute,
                private loader: AppLoaderService) {
    }

    ngOnInit(): void {
        this.opcionesDeBusqueda = OpcionesBusquedaProcesosFinalizados
        this.activatedRoute.params.subscribe(parameter => {
            const id = parameter.id
            if (id) {
                this.iniciarDetalle(id);
            } else {
                this.iniciarBuscador();
            }
        });
    }

    iniciarBuscador() {
        this.propiedades = {esBuscador: true}
        this.buildItemForm();
        this.obtenerProcesosElectoralesCombo();
    }


    iniciarDetalle(idProceso) {
        this.propiedades = {idProceso: idProceso}
        this.obtenerDatos(idProceso);
    }

    buildItemForm() {
        this.itemForm = this.fb.group({
            procesoElectoral: [''],
            nombreProcesoElectoral: [''],
            estado: [Auditoria.ACTIVO],
            anioSeleccionado: [''],
        });
    }

    obtenerDatos(idProceso) {
        this.loader.open();
        forkJoin([this.crudService.post({id: idProceso}, ProcesoElectoral.ObtenerProcesoElectoralById),
            this.crudService.post({id: idProceso}, Votos.ObtenerResumenVotos)
        ]).pipe(
            takeUntil(this.unsuscribe$)
        ).subscribe(
            ([proceso, resumen]) => {
                this.proceso = this.util.obtenerListaMensaje(proceso, true);
                this.detalleProceso = this.util.obtenerListaMensaje(resumen, true);
                if(this.proceso.tipoEleccion == this.eleccionPluripersonal && this.detalleProceso.DetallesListas)
                {
                    for (let i = 1; i <= this.detalleProceso.DetallesListas[0]?.Coeficientes?.length; i++) {
                        this.divisibles.push(i);
                    }
                    this.obtenerCandidatosListasGanadoras();
                }


                this.loader.close();
            },
            () => {
            this.loader.close();
            })
    }


    obtenerProcesosElectoralesCombo() {
        this.loader.open()
        this.crudService.post({}, ProcesoElectoral.ObtenerProcesosActivos).pipe(
            takeUntil(this.unsuscribe$)
        ).subscribe(
            (result:ApiResponseMessageModel) => {
                if(result.responseList != null){

                    this.lsProcesos = this.util.obtenerListaMensaje(result);
                }
                else{
                    this.util.mostrarMensaje(result);
                }
                this.loader.close()
            },
            ()=>
            {
                this.loader.close();
            }
        )
    }

    getItems() {
        this.loader.open();
        const data = this.itemForm.value;

        if (!data.estado) {
            data.estado = Auditoria.ACTIVO
        }
        this.crudService.post({
            parametroBusqueda1: data.nombreProcesoElectoral,
            parametroBusqueda2: data.estado,
            parametroBusqueda4: data.anioSeleccionado,
            bdt:true // bandera para filtrar solo finalizados
        }, ProcesoElectoral.ObtenerProcesosElectoralesByParametros).
            pipe(
                takeUntil(this.unsuscribe$)
        )
            .subscribe(
            (result: ApiResponseMessageModel) => {
                this.lsProcesos = this.util.obtenerListaMensaje(result);
                this.util.mostrarMensaje(result)
                this.loader.close();
            },
            () => {
                this.loader.close();
            }
        )
    }


    limpiar() {
        this.buildItemForm();
        this.parametroDeBusquedaSeleccionado = undefined;
    }

    obtenerCandidatosListasGanadoras(){
        this.detalleProceso?.DetallesListas?.forEach(
        x =>{
            const aux = []
            x.Candidatos?.forEach(y => {
                if(y.ganadorSugerido || y.ganadorSugeridoEmpate)
                {
                    aux.push(y);
                }
            })
            if(aux?.length > 0)
            this.listaGanadores.push({NombreLista: x.NombreLista, ganadores: aux})
        });

        this.listaGanadores?.forEach(x => {
            const aux = []
            x.ganadores?.forEach(y => {
                if(!y.elegible)
                {
                    aux.push(y);
                }
            })
            if(aux?.length > 0)
                this.listaGanadoresNoCumplenMinimo.push({NombreLista: x.NombreLista, ganadores: aux})
        });
    }
    ngOnDestroy() {
        this.unsuscribe$.next();
        this.unsuscribe$.complete();
    }

}
