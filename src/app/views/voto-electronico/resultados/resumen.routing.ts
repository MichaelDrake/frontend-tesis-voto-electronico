import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ResultadosComponent} from './resultados/resultados.component';
import {RutasDinamicasPorRolGuardGuard} from '../../../shared/guards/rutas-dinamicas-rol-guard';

const CatalogosRoutes: Routes = [
    {
        path: '',
        redirectTo: 'proceso',
    },
    {
        path: 'proceso',
        data:{
            title: 'Proceso',
            breadcrumb: 'PROCESO',
        },
        canActivate:[RutasDinamicasPorRolGuardGuard],
        children: [
            {
                path: '',
                redirectTo: 'finalizados',
            },
            {
                path: 'detalle/:id',
                component: ResultadosComponent,
                data: {
                    title: 'Detalle',
                    breadcrumb: 'DETALLE',
                }
            },
            {
                path: 'finalizados',
                component: ResultadosComponent,
                data: {
                    title: 'Finalizados',
                    breadcrumb: 'FINALIZADOS',
                },
            },
        ]
    }

];

@NgModule({
    imports: [RouterModule.forChild(CatalogosRoutes)],
    exports: [RouterModule],
})
export class ReusmenRouting {
}
