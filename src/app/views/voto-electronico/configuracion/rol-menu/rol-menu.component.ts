import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {CrudService} from '../../../../shared/services/facade-services/crud.service';
import {UtilService} from '../../../../shared/services/util/util.service';
import {AppLoaderService} from '../../../../shared/services/app-loader/app-loader.service';
import {Modulos, Permiso, Roles} from '../../../constantes/apis';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
    selector: 'app-rol-menu',
    templateUrl: './rol-menu.component.html',
    styleUrls: ['./rol-menu.component.less']
})
export class RolMenuComponent implements OnInit, OnDestroy {
    lsRoles: any[] = [];
    lsRolesB: any[] = [];
    lsModulos: any[] = [];
    lsRutas: any[] = [];
    itemForm: FormGroup;
    itemFormRuta: FormGroup;
    private unsuscribe$ = new Subject<void>(); //marca como completado el observable

    constructor(
        private fb: FormBuilder,
        private crudService: CrudService,
        private util: UtilService,
        private loader: AppLoaderService
    ) {
    }

    ngOnInit(): void {
        this.buildItemForm();
        this.buildItemFormRuta();
        this.getRoles();
        this.getModulos();
        this.getRutas();
    }
    ngOnDestroy() {
        this.unsuscribe$.next();
        this.unsuscribe$.complete();
    }

    buildItemForm() {
        this.itemForm = this.fb.group({
            nombreRol: [''],
            estado: [{value: 'ACTIVO', disabled: true}]
        })
    }

    buildItemFormRuta() {
        this.itemFormRuta = this.fb.group({
            nombreRol: [''],
            estado: [{value: 'ACTIVO', disabled: true}]
        })
    }

    getModulos() {
        this.crudService.post({}, Modulos.ObtenerModulosActivos)
            .pipe(
                takeUntil(this.unsuscribe$)
            ).subscribe(
            (response) => {
                this.lsModulos = this.util.obtenerListaMensaje(response);
            }
        )
    }

    getRutas() {
        this.crudService.post({}, Modulos.ObtenerRutasActivas)
            .pipe(
                takeUntil(this.unsuscribe$)
            ).subscribe(
            (response) => {
                this.lsRutas = this.util.obtenerListaMensaje(response);
            }
        )
    }

    getRoles(esRuta?, noLoader?) {
        if (!noLoader) {
            this.loader.open();
        }
        const data = esRuta ? this.itemFormRuta.value : this.itemForm.value;
        esRuta ? this.lsRolesB = [] : this.lsRoles = [];

        this.crudService.post({
            nombreRol: data.nombreRol,
            estado: data.estado
        }, esRuta ? Roles.ObtenerRolesNombreNoAdminRuta : Roles.ObtenerRolesNombreNoAdmin)
            .pipe(
                takeUntil(this.unsuscribe$)
            ).subscribe(
            (response) => {
                esRuta ? this.lsRolesB = this.util.obtenerListaMensaje(response) :
                    this.lsRoles = this.util.obtenerListaMensaje(response);
                if (!noLoader) {
                    setTimeout(() => this.loader.close());
                }
            }
            ,
            () => {
                if (!noLoader) {
                    setTimeout(() => this.loader.close());
                }
            }
        )
    }

    compareFn(a: any, b: any) {
        return a.id === b.id;
    }

    Guardar(row, esRuta?) {
        this.crudService.post({
            rolId: row.id,
            recursos: row.recursos?.map(x => ({id: x.id})),
            esRuta: esRuta
        }, Permiso.CrearPermiso)
            .pipe(
                takeUntil(this.unsuscribe$)
            ).subscribe(
            (response) => {
                this.util.mostrarMensaje(response)
                this.getRoles(esRuta, true)
            }
        )
    }

}
