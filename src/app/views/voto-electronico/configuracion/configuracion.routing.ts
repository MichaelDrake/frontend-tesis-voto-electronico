import { NgModule } from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
// import {ProcesoComponent} from './proceso/proceso.component';
import {EscanioComponent} from './administrar-elecciones/escanio/escanio.component';
import {EleccionComponent} from './administrar-elecciones/eleccion/eleccion.component';
import {RolMenuComponent} from './rol-menu/rol-menu.component';
import {PersonaComponent} from './persona/persona.component';
import {UsuarioComponent} from './usuario/usuario.component';
import {RutasPorRolGuardGuard} from '../../../shared/guards/rutas-por-rol-guard.guard';

const CatalogosRoutes: Routes = [
    {
        path: "",
        redirectTo: "eleccion"
    },
    {
        path: "escanio",
        component: EscanioComponent,
        canActivate:[RutasPorRolGuardGuard],
        data: {
            title: "Escaño",
            breadcrumb: "ESCAÑO",
        },
    },
    {
        path: "eleccion",
        component: EleccionComponent,
        canActivate:[RutasPorRolGuardGuard],
        data: {
            title: "Eleccion",
            breadcrumb: "ELECCION",
        },
    },
    {
        path: "permisos",
        component: RolMenuComponent,
        canActivate:[RutasPorRolGuardGuard],
        data: {
            title: "Permisos",
            breadcrumb: "PERMISOS",
        },
    },
    {
        path: "persona",
        component: PersonaComponent,
        canActivate:[RutasPorRolGuardGuard],
        data: {
            title: "Persona",
            breadcrumb: "PERSONA",
        },
    },
    {
        path: "usuario",
        component: UsuarioComponent,
        canActivate:[RutasPorRolGuardGuard],
        data: {
            title: "usuario",
            breadcrumb: "USUARIO",
        },
    },
];

@NgModule({
    imports: [RouterModule.forChild(CatalogosRoutes)],
    exports: [RouterModule],
})
export class ConfiguracionRoutingModule {}
