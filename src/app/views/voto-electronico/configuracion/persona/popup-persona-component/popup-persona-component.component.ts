import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {CustomValidators} from 'ngx-custom-validators';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Persona, ProcesoElectoral} from '../../../../constantes/apis';
import {Auditoria} from '../../../../constantes/variables-globales';
import {ApiResponseMessageModel} from '../../../../../shared/models/voto-electronico/api-response-message.model';
import {CrudService} from '../../../../../shared/services/facade-services/crud.service';
import {UtilService} from '../../../../../shared/services/util/util.service';
import {AppLoaderService} from '../../../../../shared/services/app-loader/app-loader.service';

@Component({
  selector: 'app-popup-persona-component',
  templateUrl: './popup-persona-component.component.html',
  styleUrls: ['../persona.component.less']
})
export class PopupPersonaComponentComponent implements OnInit {

  formData = {}
  console = console;
  itemForm: FormGroup;
  title='';

  constructor(
      @Inject(MAT_DIALOG_DATA) public data: any,
      public dialogRef: MatDialogRef<PopupPersonaComponentComponent>,
      private fb: FormBuilder,
      private crudService: CrudService,
      private util: UtilService,
      private loader: AppLoaderService,
  ) { }

  ngOnInit() {
    this.title = this.data.isNew ? 'Registro de persona' : 'Actualizar datos de persona';
    this.buildItemForm(this.data.payload);
  }

  buildItemForm(data: any = {}){

    this.itemForm = this.fb.group({
      Id: [data.id || ''],
      nombreUno: new FormControl(data.nombreUno || '', [
        Validators.required,
          Validators.maxLength(100)
      ]),
      nombreDos: new FormControl(data.nombreDos ||'', [Validators.maxLength(99)]),
      apellidoUno: new FormControl(data.apellidoUno ||'', [
        Validators.required,
        Validators.maxLength(100)
      ]),
      apellidoDos: new FormControl(data.apellidoDos ||'' , [Validators.maxLength(99)]),
      identificacion: new FormControl(data.dt1 ||'' , [
        Validators.required,
        CustomValidators.digits,
          Validators.maxLength(20)
      ]),
      telefonoUno: new FormControl(data.telefonoUno ||'', [
        Validators.required,
        CustomValidators.digits,
          Validators.maxLength(20)
      ]),
      telefonoDos: new FormControl(data.telefonoDos ||'',[
        CustomValidators.digits,
        Validators.maxLength(20)
      ]),
      email: new FormControl(data.email ||'', [
        Validators.required,
        Validators.email,
        Validators.maxLength(60)
      ]),

    })

    this.data.isNew ? this.itemForm.controls['identificacion'].enable() : this.itemForm.controls['identificacion'].disable();
    //this.data.isNew ? this.itemForm.controls['email'].enable() : this.itemForm.controls['email'].disable();
  }

  guardarDatosPersona(){

    this.loader.open();
    let dto = this.itemForm.value
    dto.dt1 = dto.identificacion


    const consulta = this.data.isNew ? this.crudService.post(dto, Persona.CrearPersona) :
        this.crudService.put(dto, Persona.ActualizarPersona);

    consulta.subscribe(
        (response: ApiResponseMessageModel) => {
          if (!this.util.existenErrores(response)) {
            this.util.mostrarMensaje(response);
            if(response.responseList != null && response.responseList.length > 0){
              this.dialogRef.close(response);
            }
          }
          this.loader.close();

        },()=>{
          this.loader.close();
        }
    )

  }

}
