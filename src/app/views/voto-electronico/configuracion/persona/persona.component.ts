import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {CrudService} from '../../../../shared/services/facade-services/crud.service';
import {UtilService} from '../../../../shared/services/util/util.service';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {
    Auditoria,
    OpcionesBusquedaModuloPersona,
} from '../../../constantes/variables-globales';
import {Persona} from '../../../constantes/apis';
import {ApiResponseMessageModel} from '../../../../shared/models/voto-electronico/api-response-message.model';
import {PopupPersonaComponentComponent} from './popup-persona-component/popup-persona-component.component';
import {AppConfirmService} from '../../../../shared/services/app-confirm/app-confirm.service';
import {AppLoaderService} from '../../../../shared/services/app-loader/app-loader.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';

@Component({
    selector: 'app-persona',
    templateUrl: './persona.component.html',
    styleUrls: ['./persona.component.less']
})
export class PersonaComponent implements OnInit, OnDestroy{
    itemForm: FormGroup;
    listaPersonas: [];
    parametroDeBusquedaSeleccionado = '';
    opcionesDeBusqueda = [];
    paginaSeleccionada = 0;
    totalRegistros;
    private unsuscribe$ = new Subject<void>(); //marca como completado el observable
    constructor(
        private fb: FormBuilder,
        private crudService: CrudService,
        private util: UtilService,
        private dialog: MatDialog,
        private confirmService: AppConfirmService,
        private loader: AppLoaderService,
    ) {
    }

    ngOnInit(): void {
        this.buildItemForm();
        this.opcionesDeBusqueda = OpcionesBusquedaModuloPersona
        this.getItems();
    }

    buildItemForm() {
        this.itemForm = this.fb.group({
            usuario: [''],
            identificacionPersona: [''],
            emailPersona: [''],
            estado: [Auditoria.ACTIVO],
        })
    }
    buscar()
    {
        this.paginaSeleccionada = 0;
        this.totalRegistros = 0;
        this.listaPersonas = [];
        this.getItems();
    }

    getItems(noLoader?) {

        if(!noLoader)this.loader.open();
        const data = this.itemForm.value;

        if (!data.estado) {
            data.estado = Auditoria.ACTIVO
        }

        this.crudService.post({
            parametroBusqueda1: data.nombrePersona,
            parametroBusqueda2: data.identificacionPersona,
            parametroBusqueda3: data.emailPersona,
            parametroBusqueda6: data.estado,
            pagina: this.paginaSeleccionada
        }, Persona.ObtenerPersonasMedianteParametrosBusqueda)
            .pipe(
                takeUntil(this.unsuscribe$)
            )
            .subscribe(
            (result: ApiResponseMessageModel) => {

                if (!this.util.existenErrores(result)) {
                    if (result.responseList) {
                        this.listaPersonas = this.util.obtenerListaMensaje(result)
                        // @ts-ignore
                        this.totalRegistros = this.listaPersonas.length > 0 ? this.listaPersonas[0].total : 0;
                    } else {
                        this.listaPersonas = [];
                        this.util.mostrarMensaje(result)
                    }
                    this.loader.close();
                }
            },()=>{
                this.loader.close();
            }
        )
    }

    limpiar() {
        this.parametroDeBusquedaSeleccionado = '';
        this.itemForm.reset();
    }


    openPoup(data: any = {}, isNew?: boolean) {

        let dialogRef: MatDialogRef<any> = this.dialog.open(
            PopupPersonaComponentComponent,
            {
                width: '500px',
                disableClose: true,
                data: {payload: data, isNew: isNew}
            }
        );
        dialogRef.afterClosed().subscribe(res => {

            if (!res) {
                // If user press cancel
                return;
            }

            this.getItems();

        });

    }

    deleteItem(row) {
        this.confirmService.confirm({
            title: 'Confirmar',
            message: 'Se eliminará la persona seleccionada. ¿Desea de continuar?'
        })
            .subscribe(respuestaOK => {
                if (respuestaOK) {
                    this.servicioEliminarPersona(row);
                }
            })
    }

    servicioEliminarPersona(row){
        this.crudService.delete({
            idPersona: row.id
        }, Persona.EliminarPersona)
            .pipe(
                takeUntil(this.unsuscribe$)
            )
            .subscribe(
            (response: ApiResponseMessageModel) => {
                if (!this.util.existenErrores(response)) {

                    this.util.mostrarMensaje(response)
                    if (response.responseList != null) {
                        this.getItems();
                    }

                }

            }
        )
    }

    habilitarItem(row) {
        this.crudService.post({
            id: row.id
        }, Persona.ReactivarPersoan)
            .pipe(
                takeUntil(this.unsuscribe$)
            )
            .subscribe(
            (response) => {
                if (!this.util.existenErrores(response)) {

                    this.util.mostrarMensaje(response)
                    if (response.responseList != null) {
                        this.getItems();
                    }}}


        )
    }
    setPage(event)
    {
        this.listaPersonas = [];
        this.paginaSeleccionada = event.offset;
        this.getItems()
    }
    ngOnDestroy() {
        this.unsuscribe$.next();
        this.unsuscribe$.complete();
    }


}
