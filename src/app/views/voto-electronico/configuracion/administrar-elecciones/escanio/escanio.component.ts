import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {CrudService} from '../../../../../shared/services/facade-services/crud.service';
import {Escanio} from '../../../../constantes/apis';
import {UtilService} from '../../../../../shared/services/util/util.service';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {PopupEscanioComponent} from './popup-escanio/popup-escanio.component';

@Component({
    selector: 'app-escanio',
    templateUrl: './escanio.component.html',
    styleUrls: ['./escanio.component.less']
})
export class EscanioComponent implements OnInit {
    itemForm: FormGroup;
    escanios: [];

    constructor(
        private fb: FormBuilder,
        private crudService: CrudService,
        private util: UtilService,
        private dialog: MatDialog,
    ) {
    }


    ngOnInit(): void {
        this.buildItemForm();
    }

    buildItemForm() {
        this.itemForm = this.fb.group({
            nombreEscanio: [''],
            estado: ['']
        })
    }

    getItems() {
        const data = this.itemForm.value;
        this.crudService.post({
            nombreEscanio: data.nombreEscanio,
            estado: data.estado
        }, Escanio.ObtenerEscanios).subscribe(
            (result) => {
                this.escanios = this.util.obtenerListaMensaje(result).map(
                    (escanio:any)=>{
                        escanio.badge = escanio.estado == "ACTIVO" ? true:false;
                        return escanio
                    }
                );
                this.util.mostrarMensaje(result)
            }
        )
    }

    openPoup(data: any = {}, isNew?: boolean) {
        let title = isNew ? 'Agregar ' : 'Actualizar ' + data.dt1;
        let dialogRef: MatDialogRef<any> = this.dialog.open(
            PopupEscanioComponent,
            {
                width: '400px',
                disableClose: true,
                data: {title: title, payload: data}
            }
        );
        dialogRef.afterClosed().subscribe(res => {
            if (!res) {
                // If user press cancel
                return;
            }
            const consulta = isNew? this.crudService.post({
                nombreEscanio: res.nombreEscanio,
                eleccionId: res.eleccion,
                estado: "ACTIVO"
            },  Escanio.CrearEscanio ):
                this.crudService.put({
                id: data.id,
                nombreEscanio: res.nombreEscanio,
                eleccionId: res.eleccion,
            },  Escanio.ActualizarEscanio );

            consulta.subscribe(
                (resp) => {
                    this.util.mostrarMensaje(resp);
                    this.getItems();
                }
            )
        });

    }

    deleteItem(row)
    {
        this.crudService.put({
            id: row.id,
            nombreEscanio: row.nombreEscanio,
            eleccionId: row.eleccionId,
            estado: "INACTIVO"
        }, Escanio.ActualizarEscanio).subscribe(
            (resp)=>
            {
                this.util.mostrarMensaje(resp);
                this.getItems();
            }
        )
    }
}
