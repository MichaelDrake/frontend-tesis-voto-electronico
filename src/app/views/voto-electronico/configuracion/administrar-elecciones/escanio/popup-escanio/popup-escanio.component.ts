import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CrudService} from '../../../../../../shared/services/facade-services/crud.service';
import {Eleccion} from '../../../../../constantes/apis';
import {UtilService} from '../../../../../../shared/services/util/util.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-popup-escanio',
  templateUrl: './popup-escanio.component.html',
  styleUrls: ['../escanio.component.less']
})
export class PopupEscanioComponent implements OnInit, OnDestroy {

  itemForm: FormGroup;
  lsElecciones:any[] = [];
  private unsuscribe$ = new Subject<void>(); //marca como completado el observable

  constructor(
      @Inject(MAT_DIALOG_DATA)
      public data: any,
      public dialogRef: MatDialogRef<PopupEscanioComponent>,
      private fb : FormBuilder,
      private crudService: CrudService,
      private util: UtilService,
  ) { }

  ngOnInit(): void {
    this.buildItemForm(this.data.payload);
    this.getElecciones();
  }
  /***
   * Elimina todas las subscribciones
   * de los observables
   */
  ngOnDestroy(): void {
    this.unsuscribe$.next(); // enviar el siguiente dato
    this.unsuscribe$.complete(); // marca como completado al observable
  }

  buildItemForm(data:any = {})
  {
    this.itemForm = this.fb.group({
      nombreEscanio: [data.nombreEscanio || "", Validators.required],
      eleccion:[data.eleccionId || "",Validators.required]
    })

  }

  getElecciones(){
    this.crudService.post({
      nombreEscanio:[''],
      estado:"ACTIVO"
    }, Eleccion.ObtenerEleccionByParametros)
        .pipe(
            takeUntil(this.unsuscribe$)
        ).subscribe(
        (response)=>{
          this.lsElecciones = this.util.obtenerListaMensaje(response)
        }
    )


  }
}
