import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {CrudService} from '../../../../../../shared/services/facade-services/crud.service';
import {TipoEleccion} from '../../../../../constantes/apis';
import {UtilService} from '../../../../../../shared/services/util/util.service';
import {Auditoria} from '../../../../../constantes/variables-globales';
import {ApiResponseMessageModel} from '../../../../../../shared/models/voto-electronico/api-response-message.model';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
    selector: 'app-popup-eleccion',
    templateUrl: './popup-eleccion.component.html',
    styleUrls: ['../eleccion.component.less']
})
export class PopupEleccionComponent implements OnInit {

    itemForm: FormGroup;
    lsTipoEleccion: any[] = [];
    esPluripersonal: boolean = false;
    mostrarAplicas: boolean = false;
    private unsuscribe$ = new Subject<void>(); //marca como completado el observable
    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        public dialogRef: MatDialogRef<PopupEleccionComponent>,
        private fb: FormBuilder,
        private crudService: CrudService,
        private util: UtilService,
    ) {
    }

    ngOnInit(): void {
        this.getTipoElecciones();
        this.buildItemForm(this.data.payload);
        this.registerEvents();
    }
    /***
     * Elimina todas las subscribciones
     * de los observables
     */
    ngOnDestroy(): void {
        this.unsuscribe$.next(); // enviar el siguiente dato
        this.unsuscribe$.complete(); // marca como completado al observable
    }
    buildItemForm(data: any = {}) {
        this.itemForm = this.fb.group({
            nombreEleccion: [data.nombreEleccion || '', Validators.required],
            aplicaAlterno:[data.aplicaAlterno || false],
            aplicaAlicuota:[data.aplicaAlicuota || false],
            tipoEleccionId: [{value:data.tipoEleccionId, disabled:!this.data.isNew } || '', Validators.required],
            cantidadEscanios: [data.cantidadEscanios || ''],
            etiquetaEscanios: [data.etiquetaEscanios || '']
        })
    }

    getTipoElecciones() {

        this.crudService.post({
            nombreTipoEleccion: '',
            estado: Auditoria.ACTIVO
        }, TipoEleccion.ObtenerEleccionByParametros)
            .pipe(
                takeUntil(this.unsuscribe$)
            ).subscribe(
            (result:ApiResponseMessageModel) => {
                if (!this.util.existenErrores(result)) {

                    if(result.responseList != null){
                        this.lsTipoEleccion = this.util.obtenerListaMensaje(result)
                        if (!this.data.isNew && (!this.data.payload.cantidadEscanios || !this.data.etiquetaEscanios))
                            this.gestionarTipoEleccion(this.data.payload.tipoEleccionId);
                    }else{
                        this.util.mostrarMensaje(result);
                    }
                    this.mostrarAplicas = true;
                }

            }
        )

    }

    registerEvents() {
        this.itemForm.controls['tipoEleccionId'].valueChanges.subscribe(
            (v) => {
                this.vaciarBanderas();
                if (v)this.gestionarTipoEleccion(v)
            }
        )
    }
    vaciarBanderas()
    {
        this.itemForm.controls["aplicaAlicuota"].reset();
        this.itemForm.controls["aplicaAlterno"].reset();
    }
    gestionarTipoEleccion(tipoEleccionId)
    {
        const a = this.lsTipoEleccion.find(x => x.id == tipoEleccionId);
        if (a.nombreTipoEleccion == 'Pluripersonal') {
            this.esPluripersonal = true;
            this.itemForm.controls['cantidadEscanios'].setValidators([Validators.required]);
            this.itemForm.controls['cantidadEscanios'].updateValueAndValidity();
            this.itemForm.controls['etiquetaEscanios'].setValidators([Validators.required]);
            this.itemForm.controls['etiquetaEscanios'].updateValueAndValidity();
        } else {
            this.esPluripersonal = false;
            this.itemForm.controls['cantidadEscanios'].reset();
            this.itemForm.controls['etiquetaEscanios'].reset();
            this.itemForm.controls['cantidadEscanios'].setValidators(null);
            this.itemForm.controls['cantidadEscanios'].updateValueAndValidity();
            this.itemForm.controls['etiquetaEscanios'].setValidators(null);
            this.itemForm.controls['etiquetaEscanios'].updateValueAndValidity();
        }
    }
}
