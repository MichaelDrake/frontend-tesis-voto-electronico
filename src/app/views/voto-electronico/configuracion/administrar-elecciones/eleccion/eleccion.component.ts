import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {CrudService} from '../../../../../shared/services/facade-services/crud.service';
import {UtilService} from '../../../../../shared/services/util/util.service';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {Eleccion, Escanio} from '../../../../constantes/apis';
import {PopupEleccionComponent} from './popup-eleccion/popup-eleccion.component';
import {egretAnimations} from '../../../../../shared/animations/egret-animations';
import {PopupEscanioComponent} from '../escanio/popup-escanio/popup-escanio.component';
import {AppLoaderService} from '../../../../../shared/services/app-loader/app-loader.service';
import {AppConfirmService} from '../../../../../shared/services/app-confirm/app-confirm.service';
import {ApiResponseMessageModel} from '../../../../../shared/models/voto-electronico/api-response-message.model';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';


@Component({
    selector: 'app-eleccion',
    templateUrl: './eleccion.component.html',
    styleUrls: ['./eleccion.component.less'],
    animations: egretAnimations
})
export class EleccionComponent implements OnInit, OnDestroy {
    itemForm: FormGroup;
    elecciones: [];
    tabSelected = new FormControl(0);
    eleccionSeleccionada: any;
    stringPluripersonal = 'Pluripersonal'
    lsEscanios: [];
    private unsuscribe$ = new Subject<void>(); //marca como completado el observable
    constructor(
        private fb: FormBuilder,
        private crudService: CrudService,
        private util: UtilService,
        private dialog: MatDialog,
        private loader: AppLoaderService,
        private confirmService: AppConfirmService,
    ) {
    }


    ngOnInit(): void {
        this.buildItemForm();
        this.getItems();
    }

    /***
     * Elimina todas las subscribciones
     * de los observables
     */
    ngOnDestroy(): void {
        this.unsuscribe$.next(); // enviar el siguiente dato
        this.unsuscribe$.complete(); // marca como completado al observable
    }

    buildItemForm() {
        this.itemForm = this.fb.group({
            nombreEleccion: [''],
            tipoEleccionId: [''],
            estado: [null]
        })
    }

    submit() {
        this.tabSelected.setValue(0);
        this.getItems();
    }

    getItems(noLoader?) {
        if(!noLoader)this.loader.open();
        this.lsEscanios = [];
        this.eleccionSeleccionada = undefined

        const data = this.itemForm.value;
        this.crudService.post({
            nombreEleccion: data.nombreEleccion,
            estado: data.estado ?? 'ACTIVO'
        }, Eleccion.ObtenerEleccionByParametros)
            .pipe(
                takeUntil(this.unsuscribe$)
            ).subscribe(
            (result: ApiResponseMessageModel) => {


                if (!this.util.existenErrores(result)) {

                    if(result.responseList != null) {
                        this.elecciones = this.util.obtenerListaMensaje(result).map(
                            (eleccion: any) => {
                                eleccion.badge = eleccion.estado == 'ACTIVO' ? true : false;
                                return eleccion
                            }
                        );
                    }else{
                        this.elecciones = [];
                        this.util.mostrarMensaje(result)
                    }

                }
                this.loader.close()

            },
            () => {
                if(!noLoader)setTimeout(() => this.loader.close())
            }
        )
    }

    openPopup(data: any = {}, isNew?: boolean) {

        let title = isNew ? 'Crear elección  ' : 'Actualizar elección ';
        let dialogRef: MatDialogRef<any> = this.dialog.open(
            PopupEleccionComponent,
            {
                width: '500px',
                disableClose: true,
                data: {title: title, payload: data, isNew: isNew}
            }
        );
        dialogRef.afterClosed().subscribe(res => {
            if (!res) {
                // If user press cancel
                return;
            }
            this.loader.open();
            const consulta = isNew ? this.crudService.post({
                    nombreEleccion: res.nombreEleccion,
                    tipoEleccionId: res.tipoEleccionId,
                    etiquetaEscanios: res.etiquetaEscanios,
                    cantidadEscanios: res.cantidadEscanios,
                    aplicaAlterno: res.aplicaAlterno,
                    aplicaAlicuota: res.aplicaAlicuota
                }, Eleccion.CrearEleccion) :
                this.crudService.put({
                    id: data.id,
                    nombreEleccion: res.nombreEleccion,
                    etiquetaEscanios: res.etiquetaEscanios,
                    cantidadEscanios: res.cantidadEscanios,
                    aplicaAlterno: res.aplicaAlterno,
                    aplicaAlicuota: res.aplicaAlicuota
                }, Eleccion.ActualizarEleccion);

            consulta .pipe(
                takeUntil(this.unsuscribe$)
            ).subscribe(
                (resp) => {
                    this.loader.close();
                    this.util.mostrarMensaje(resp);
                    this.getItems(true);
                },
                ()=>
                {
                    this.loader.close();
                }
            )
        });

    }


    onTabChange(event) {
        window.dispatchEvent(new Event('resize'));
        if(event.index === 0) {
        this.eleccionSeleccionada = undefined;
        }
    }

    verEscanios(row) {
        setTimeout(()=>{
            this.lsEscanios = [];
            this.eleccionSeleccionada = {...row};
            this.getEscanios(row.id)
        })

        setTimeout(()=>this.tabSelected.setValue(1));
    }

    getEscanios(eleccionId, noLoader?) {
        if(!noLoader)this.loader.open();

        this.crudService.post({id: eleccionId}, Escanio.ObtenerEscaniosPorEleccion)
            .pipe(
                takeUntil(this.unsuscribe$)
            ).subscribe(
            (response) => {
                this.lsEscanios = this.util.obtenerListaMensaje(response);
                if(!noLoader)this.loader.close();
            },
            ()=>
            {
                this.lsEscanios = [];
                if(!noLoader)this.loader.close();
            }
        )
    }

    openPopupEscanios(data: any = {}, isNew?: boolean) {
        let title = isNew ? 'Agregar escaño a:"' + this.eleccionSeleccionada.nombreEleccion + '"' : 'Actualizar ' + data.nombreEleccion;
        let dialogRef: MatDialogRef<any> = this.dialog.open(
            PopupEscanioComponent,
            {
                width: '500px',
                disableClose: true,
                data: {title: title, payload: data}
            }
        );
        dialogRef.afterClosed().subscribe(res => {
            if (!res) {
                // If user press cancel
                return;
            }
            this.loader.open();
            const consulta = isNew ? this.crudService.post({
                    nombreEscanio: res.nombreEscanio,
                    eleccionId: this.eleccionSeleccionada?.id,
                    estado: 'ACTIVO'
                }, Escanio.CrearEscanio) :
                this.crudService.put({
                    escanioId: data.escanioId,
                    nombreEscanio: res.nombreEscanio,
                    eleccionId: res.eleccion,
                }, Escanio.ActualizarEscanio);

            consulta .pipe(
                takeUntil(this.unsuscribe$)
            ).subscribe(
                (resp) => {
                    this.util.mostrarMensaje(resp);
                    this.getEscanios(this.eleccionSeleccionada.id, true);
                    this.loader.close();
                },
                ()=>
                {
                    this.loader.close();
                }
            )
        });

    }

    deleteItem(row) {
        this.confirmService.confirm({
            title: 'Confirmar',
            message: 'Se eliminará la elección seleccionada. ¿Desea de continuar?'
        })
            .subscribe(respuestaOK => {
                if (respuestaOK) {
                    this.servicioEliminarEleccion(row);
                }
            })
    }


    deleteItemEscanio(row) {
        this.confirmService.confirm({
            title: 'Confirmar',
            message: 'Se eliminará el escaño seleccionado. ¿Desea de continuar?'
        })
            .subscribe(respuestaOK => {
                if (respuestaOK) {
                    this.servicioEliminarEscanio(row);
                }
            })
    }

    servicioEliminarEscanio(row) {
        this.loader.open();
        this.crudService.put({
            escanioId: row.escanioId,
        }, Escanio.EliminarEscanio)
            .pipe(
                takeUntil(this.unsuscribe$)
            ).subscribe(
            (resp) => {
                this.loader.close();
                this.util.mostrarMensaje(resp);
                this.getEscanios(this.eleccionSeleccionada?.id, true);
            },
            ()=>
            {
                this.loader.close();
            }
        )
    }

    servicioEliminarEleccion(row) {
        this.loader.open();
        this.crudService.put({
            id: row.id,
        }, Eleccion.EliminarEleccion)
            .pipe(
                takeUntil(this.unsuscribe$)
            ).subscribe(
            (resp) => {
                this.loader.close();
                this.util.mostrarMensaje(resp);
                this.getItems(true);
            },
            ()=>
            {
                this.loader.close();
            }
        )
    }

    reactivar(row) {
        this.loader.open();
        this.crudService.post({id: row.id}, Eleccion.ReactivarEleccion)
            .pipe(
                takeUntil(this.unsuscribe$)
            ).subscribe(
            (response) => {
                this.loader.close();
                this.util.mostrarMensaje(response);
                this.lsEscanios = this.util.obtenerListaMensaje(response);
                this.getItems(true)
            }
        )

    }

    reactivarEscanio(row) {
        this.loader.open();
        this.crudService.post({escanioId: row.escanioId}, Escanio.ReactivarEscanios)
            .pipe(
                takeUntil(this.unsuscribe$)
            ).subscribe(
            (response) => {
                this.util.mostrarMensaje(response);
                this.lsEscanios = this.util.obtenerListaMensaje(response);
                this.getEscanios(this.eleccionSeleccionada?.id, true);
                this.loader.close();
            }
            ,
            ()=>
            {
                this.loader.close();
            }
        )
    }
}
