import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {CrudService} from '../../../../../shared/services/facade-services/crud.service';
import {UtilService} from '../../../../../shared/services/util/util.service';
import {CustomValidators} from 'ngx-custom-validators';
import {Cargo, Escanio, Persona, Roles, Usuario, UsuarioRol} from '../../../../constantes/apis';
import {ApiResponseMessageModel} from '../../../../../shared/models/voto-electronico/api-response-message.model';
import {Ng2ImgMaxService} from 'ng2-img-max';
import {Auditoria} from '../../../../constantes/variables-globales';
import {AppLoaderService} from '../../../../../shared/services/app-loader/app-loader.service';

@Component({
  selector: 'app-popup-usuario-component',
  templateUrl: './popup-usuario-component.component.html',
  styleUrls: ['../usuario.component.less']
})
export class PopupUsuarioComponentComponent implements OnInit {

  formData = {}
  itemForm: FormGroup;
  title='';
  listaRoles=[];
  listaCargos=[];

  constructor(
      @Inject(MAT_DIALOG_DATA) public data: any,
      public dialogRef: MatDialogRef<PopupUsuarioComponentComponent>,
      private fb: FormBuilder,
      private crudService: CrudService,
      private util: UtilService,
      private ng2ImgMaxService: Ng2ImgMaxService,
      private loader: AppLoaderService,
  ) { }

  ngOnInit() {
    this.title = this.data.isNew ? 'Nuevo usuario' : 'Actualizar datos de usuario';

    this.buildItemForm(this.data.payload);
    this.obtenerRolesDisponibles();
    this.obtenerCargosDisponibles();
  }

  buildItemForm(data: any = {}){

    this.itemForm = this.fb.group({
      usuario: new FormControl(data || '' , [
        Validators.required,
      ]),
      rolUsuario: [data.rolId || '', Validators.required],
      cargoUsuario: [data.cargoId || '', Validators.required],
      identificacion:[data.nombreUsuario || ''],
      nombrePersona:[data.nombreUsuario || ''],
    })

    this.itemForm.controls['identificacion'].disable();
    this.itemForm.controls['nombrePersona'].disable();

  }

  crearUsuario(){
    this.loader.open();

    if (this.itemForm.value['usuario'].dt1 == null){
        this.loader.close();
        return;
    }
    else{
      let dto = {
        nombreUsuario : this.itemForm.value['usuario'].dt1,
        PersonaId : this.itemForm.value['usuario'].id,
        rolId: this.itemForm.value['rolUsuario'],
        cargoId: this.itemForm.value['cargoUsuario']
      }

      const consulta = this.crudService.post(dto, Usuario.CrearUsuario)

      consulta.subscribe(
          (response: ApiResponseMessageModel) => {
            if (!this.util.existenErrores(response)) {
              this.util.mostrarMensaje(response);
              if(response.responseList != null && response.responseList.length > 0){
                this.dialogRef.close(response);
              }
            }

            this.loader.close();
          },()=>{
            this.loader.close();
          }
      )
    }


  }

  realizarAccionFormulario(){
    if(this.data.isNew == true){
      this.crearUsuario()
    }else{
      this.actualizarUsuario()
    }
  }

  actualizarUsuario(){

    this.loader.open();

    if (this.itemForm.value['usuario'].idUsuario == null){
      return;
    }
    else{
      let dto = {
        rolId: this.itemForm.value['rolUsuario'],
        cargoId: this.itemForm.value['cargoUsuario'],
        idUsuario: this.itemForm.value['usuario'].idUsuario,
        estado: Auditoria.ACTIVO
      }

      const consulta = this.crudService.put(dto, Usuario.ActualizarUsuarioWeb);

      consulta.subscribe(
          (response: ApiResponseMessageModel) => {
            if (!this.util.existenErrores(response)) {
              this.util.mostrarMensaje(response);
              if(response.responseList != null && response.responseList.length > 0){
                this.dialogRef.close(response);
              }
            }
            this.loader.close();

          },()=>{
            this.loader.close();
      }
      )
    }


  }

  /**
   * maneja las imagenes que se cargan
   * @param event
   */
  onSelectFile(event) {

    const extensionImg = event.target.files[0];
    if (event.target.files && event.target.files[0]) {
      // this.cargandoImagen = true
      let file = event.target.files[0];

      this.ng2ImgMaxService.resizeImage(file, 720
          , 450).subscribe(result => {
        var reader = new FileReader();

        reader.onload = (event: any) => {
          const logo = {
            base64: event.target.result,
            tipo: extensionImg.type,
            extension: '.' + extensionImg.name.split('.')[1],
          };
          // this.cargandoImagen = false;
          this.itemForm.controls['imagenUsuario'].setValue(logo);
          this.itemForm.controls['imagenUsuario'].updateValueAndValidity();
        };
        reader.readAsDataURL(result);

      })
    }
  }

  obtenerRolesDisponibles() {
    this.crudService.post({}, Roles.ObtenerListaRolesActivos).subscribe(
        (response:ApiResponseMessageModel) => {
          if (!this.util.existenErrores(response)) {
            if(response.responseList){
              this.listaRoles = this.util.obtenerListaMensaje(response,false)
            }
            else{
              this.util.mostrarMensaje(response);
              this.listaRoles = [];
            }

          }
        }
    )
  }

  obtenerCargosDisponibles() {
    this.crudService.post({}, Cargo.ObtenerListaCargosActivos).subscribe(
        (response:ApiResponseMessageModel) => {
          if (!this.util.existenErrores(response)) {
            if(response.responseList){
              this.listaCargos = this.util.obtenerListaMensaje(response,false)
            }
            else{
              this.util.mostrarMensaje(response);
              this.listaCargos = [];
            }

          }
        }
    )
  }


}
