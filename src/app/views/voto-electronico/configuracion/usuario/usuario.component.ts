import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {CrudService} from '../../../../shared/services/facade-services/crud.service';
import {UtilService} from '../../../../shared/services/util/util.service';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {Auditoria, OpcionesBusquedaModuloUsuario} from '../../../constantes/variables-globales';
import {Usuario} from '../../../constantes/apis';
import {ApiResponseMessageModel} from '../../../../shared/models/voto-electronico/api-response-message.model';
import {PopupUsuarioComponentComponent} from './popup-usuario-component/popup-usuario-component.component';
import {AppConfirmService} from '../../../../shared/services/app-confirm/app-confirm.service';
import {AppLoaderService} from '../../../../shared/services/app-loader/app-loader.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
    selector: 'app-usuario',
    templateUrl: './usuario.component.html',
    styleUrls: ['./usuario.component.less']
})
export class UsuarioComponent implements OnInit, OnDestroy {

    itemForm: FormGroup;
    listaPersonas:any[] =  [];
    parametroDeBusquedaSeleccionado = '';
    opcionesDeBusqueda = [];
    paginaSeleccionada = 0;
    totalRegistros;
    private unsuscribe$ = new Subject<void>(); //marca como completado el observable
    constructor(
        private fb: FormBuilder,
        private crudService: CrudService,
        private util: UtilService,
        private dialog: MatDialog,
        private confirmService: AppConfirmService,
        private loader: AppLoaderService,
    ) {
    }

    ngOnInit(): void {
        this.buildItemForm();
        this.opcionesDeBusqueda = OpcionesBusquedaModuloUsuario;
        this.getItems();
    }

    buildItemForm() {
        this.itemForm = this.fb.group({
            usuario: [''],
            estado: [Auditoria.ACTIVO],
        })
    }
    buscar()
    {
        this.paginaSeleccionada= 0;
        this.totalRegistros = 0;
        this.listaPersonas = [];
        this.getItems();
    }

    getItems(isLoader?) {
        const data = this.itemForm.value;
        if (!data.estado) {
            data.estado = Auditoria.ACTIVO
        }
        if(!isLoader)this.loader.open();
        this.crudService.post({
            parametroBusqueda1: data.usuario,
            parametroBusqueda6: data.estado,
            pagina: this.paginaSeleccionada
        }, Usuario.ObtenerListaUsuariosMedianteParametrosBusqueda)
            .pipe(
                takeUntil(this.unsuscribe$)
            )
            .subscribe(
            (result: ApiResponseMessageModel) => {
                setTimeout(()=>{this.loader.close();})
                if (!this.util.existenErrores(result)) {
                    if (result.responseList) {
                        this.listaPersonas = this.util.obtenerListaMensaje(result);
                        this.totalRegistros = this.listaPersonas.length > 0 ? this.listaPersonas[0].total : 0;
                    } else {
                        this.listaPersonas = [];
                        this.util.mostrarMensaje(result)
                    }

                }

            }, () => {
                setTimeout(()=>{this.loader.close();})
            }
        )
    }

    limpiar() {
        this.parametroDeBusquedaSeleccionado = '';
        this.itemForm.reset();
    }


    openPoup(data: any = {}, isNew?: boolean) {

        let dialogRef: MatDialogRef<any> = this.dialog.open(
            PopupUsuarioComponentComponent,
            {
                width: '500px',
                disableClose: true,
                data: {payload: data, isNew: isNew}
            }
        );
        dialogRef.afterClosed().subscribe(res => {

            if (!res) {
                // If user press cancel
                return;
            }

            this.getItems();

        });

    }

    /*ObtenerPersonaByUsuarioId(personaId: number) {
        this.crudService.post({
            idUsuario: personaId
        }, Persona.ObtenerPersonasMedianteParametrosBusqueda).subscribe(
            (response: ApiResponseMessageModel) => {
                if (!this.util.existenErrores(response)) {

                    this.util.mostrarMensaje(response)
                    if (response.responseList != null) {
                        this.getItems();
                    }

                }

            }
        )
    }*/

    InactivarUsuario(row) {
        this.confirmService.confirm({
            title: 'Confirmar',
            message: 'Se eliminará el usuario seleccionado. ¿Desea de continuar?'
        })
            .subscribe(respuestaOK => {
                if (respuestaOK) {
                    this.servicioInactivarusuario(row);
                }
            })
    }

    servicioInactivarusuario(row) {
        this.crudService.put({
            idUsuario: row?.idUsuario,
            estado: Auditoria.INACTIVO
        }, Usuario.CambiarEstadoUsuario)
            .pipe(
                takeUntil(this.unsuscribe$)
            )
            .subscribe(
            (response: ApiResponseMessageModel) => {
                if (!this.util.existenErrores(response)) {

                    this.util.mostrarMensaje(response)
                    if (response.responseList != null) {
                        this.getItems();
                    }

                }

            }
        )
    }

    habilitarUsuario(row) {
        this.crudService.put({
            idUsuario: row.idUsuario,
            estado: Auditoria.ACTIVO
        }, Usuario.CambiarEstadoUsuario)
            .pipe(
                takeUntil(this.unsuscribe$)
            ).subscribe(
            (resp) => {
                this.util.mostrarMensaje(resp);
                this.getItems();
            }
        )
    }

    ReenviarEmailActivacionUsuario(row) {

        this.confirmService.confirm({
            title: 'Confirmar',
            message: 'Se enviára un correo para el inicio de sesión del usuario. ¿Desea de continuar?'
        })
            .subscribe(respuestaOK => {
                if (respuestaOK) {
                    this.servicioenviarEmail(row);
                }
            })



    }

    servicioenviarEmail(row){

        this.loader.open();

        this.crudService.post({
            idUsuario: row.idUsuario,
            estado: Auditoria.ACTIVO
        }, Usuario.ReenviarEmailActivacion)
            .pipe(
                takeUntil(this.unsuscribe$)
            )
            .subscribe(
            (resp) => {
                this.util.mostrarMensaje(resp);
                this.getItems(true);
                //setTimeout(()=>{this.loader.close();})
                this.loader.close();
            },
            () => {
                setTimeout(()=>{this.loader.close();})
            }
        )
    }
    ngOnDestroy() {
        this.unsuscribe$.next();
        this.unsuscribe$.complete();
    }

    setPage(event)
    {
        this.listaPersonas = [];
        this.paginaSeleccionada = event.offset;
        this.getItems()
    }
}
