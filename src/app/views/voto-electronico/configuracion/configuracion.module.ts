import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatCardModule} from '@angular/material/card';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatIconModule} from '@angular/material/icon';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import {SharedModule} from '../../../shared/shared.module';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {MatSelectModule} from '@angular/material/select';
// import { ProcesoComponent } from './proceso/proceso.component';
import {ConfiguracionRoutingModule} from './configuracion.routing';
import {MatStepperModule} from '@angular/material/stepper';
// import {PopupErrorComponent} from './proceso/popup-error/popup-error.component';
import {EleccionComponent} from './administrar-elecciones/eleccion/eleccion.component';
import {EscanioComponent} from './administrar-elecciones/escanio/escanio.component';
import {PopupEleccionComponent} from './administrar-elecciones/eleccion/popup-eleccion/popup-eleccion.component';
import {MatChipsModule} from '@angular/material/chips';
import {MatTabsModule} from '@angular/material/tabs';
import {MatTooltipModule} from '@angular/material/tooltip';
import {PopupEscanioComponent} from './administrar-elecciones/escanio/popup-escanio/popup-escanio.component';
import {MatDialogModule} from '@angular/material/dialog';
import { RolMenuComponent } from './rol-menu/rol-menu.component';
import { PersonaComponent } from './persona/persona.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { PopupPersonaComponentComponent } from './persona/popup-persona-component/popup-persona-component.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatRadioModule} from '@angular/material/radio';
import {MatDividerModule} from '@angular/material/divider';
import { PopupUsuarioComponentComponent } from './usuario/popup-usuario-component/popup-usuario-component.component';
import {Ng2ImgMaxModule} from 'ng2-img-max';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MatProgressBarModule,
        MatButtonModule,
        MatInputModule,
        MatCardModule,
        MatCheckboxModule,
        MatIconModule,
        FlexLayoutModule,
        ConfiguracionRoutingModule,
        SharedModule,
        NgxDatatableModule,
        MatSelectModule,
        MatStepperModule,
        MatChipsModule,
        MatTabsModule,
        MatTooltipModule,
        MatDialogModule,
        MatDatepickerModule,
        MatRadioModule,
        MatDividerModule,
        Ng2ImgMaxModule
    ],

    declarations: [
        PopupEscanioComponent,
        EleccionComponent,
        EscanioComponent,
        // ProcesoComponent,
        // PopupErrorComponent,
        PopupEleccionComponent,
        RolMenuComponent,
        PersonaComponent,
        UsuarioComponent,
        PopupPersonaComponentComponent,
        PopupUsuarioComponentComponent
        ],

    entryComponents: [
        PopupEscanioComponent,
        // PopupErrorComponent,
        PopupEleccionComponent,
    ]
})
export class ConfiguracionModule {
}
