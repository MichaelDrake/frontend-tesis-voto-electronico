import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
//import {EscanioComponent} from '../configuracion/escanio/escanio.component';
//import {EleccionComponent} from '../configuracion/eleccion/eleccion.component';
import {ProcesoElectoralComponent} from './administrar-proceso/tab-proceso-electoral/proceso-electoral.component';
import {AdministrarProcesoComponent} from './administrar-proceso/administrar-proceso.component';
import {RutasPorRolGuardGuard} from '../../../shared/guards/rutas-por-rol-guard.guard';
import {EleccionDisponibleComponent} from './eleccion/eleccion-disponible/eleccion-disponible.component';
import {MesaDeVotoComponent} from './mesa-de-voto/mesa-de-voto.component';
import {VerificarVotoComponent} from './voto/verificar-voto/verificar-voto.component';
import {PerfilComponent} from './perfil/perfil.component';
import {ValidarAccesoMvGuard} from '../../../shared/guards/validar-acceso-mv.guard';

const CatalogosRoutes: Routes = [
    {
        path: '',
        redirectTo: 'perfil',
    },
    {
        path: 'administrar-proceso',
        component: AdministrarProcesoComponent,
        canActivate:[RutasPorRolGuardGuard],
        data: {
            title: 'Administrar Proceso',
            breadcrumb: 'Administrar Proceso',
        },
    }
    ,
    {
        path: 'eleccion-disponible',
        component: EleccionDisponibleComponent,
        canActivate:[RutasPorRolGuardGuard],
        data: {
            title: 'Elecciones disponibles',
            breadcrumb: 'Elecciones disponibles',
        },
    }
    ,
    {
        path: 'mesa-voto/:procesoElectoralId',
        component: MesaDeVotoComponent,
        canActivate:[ValidarAccesoMvGuard],
        data: {
            title: 'Mesa de voto',
            breadcrumb: 'Mesa de voto',
        },
    }
    ,
    {
        path: 'verificar-voto',
        component: VerificarVotoComponent,
        canActivate:[RutasPorRolGuardGuard],
        data: {
            title: 'Validar voto',
            breadcrumb: 'Validar voto',
        },
    }
    ,
    {
        path: 'perfil',
        component: PerfilComponent,
        canActivate:[RutasPorRolGuardGuard],
        data: {
            title: 'Mi Perfil',
            breadcrumb: 'Mi Perfil',
        },
    }
];

@NgModule({
    imports: [RouterModule.forChild(CatalogosRoutes)],
    exports: [RouterModule],
})
export class CatalogosRoutingModule {
}
