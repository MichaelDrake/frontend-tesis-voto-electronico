import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl} from '@angular/forms';
import {TabDeListaComponent} from './tab-de-lista/tab-de-lista.component';
import {TabCandidatosComponent} from './tab-candidatos/tab-candidatos.component';
import {TabImportacionPadronComponent} from './tab-importacion-padron/tab-importacion-padron.component';
import { MatTabChangeEvent } from '@angular/material/tabs';

@Component({
  selector: 'app-administrar-proceso',
  templateUrl: './administrar-proceso.component.html',
  styleUrls: ['./administrar-proceso.component.less']
})
export class AdministrarProcesoComponent implements OnInit {
  tabs = ['Proceso Electoral', 'Listas', 'Candidatos','Padron electoral'];
  tabSelected = new FormControl(0);
  procesoSelected;
  listaSelected;
  tabListaDesabilitado=true;
  tabCandidatosDesabilitado=true;
  //visualizarTabPadron = false;
  tabImportarPadronDesabilitado = true;

  @ViewChild("listaComponent", {static: false}) listaComponent:TabDeListaComponent;
  @ViewChild("candidatoComponent", {static: false}) candidatoComponent: TabCandidatosComponent;
  @ViewChild("tabPadron", {static: false}) tabPadron: TabImportacionPadronComponent;


  constructor() { }

  ngOnInit(): void {
  }

  changeSelectedTab(event){
    window.dispatchEvent(new Event('resize'));
    if(event.index === 0){
      this.tabListaDesabilitado = true;
      this.tabCandidatosDesabilitado = true;
      this.tabImportarPadronDesabilitado = true;
      //this.visualizarTabPadron = false;
    }
    if(event.index === 1){
      this.tabCandidatosDesabilitado = true;
    }
  }

  irLista(row)
  {
    this.tabSelected.setValue(1); // Indice del Tab para Listas
    this.procesoSelected = row;
    this.tabListaDesabilitado = false;
    this.listaComponent.limpiarArregloListas();
    setTimeout(()=> this.listaComponent.getItems());
  }

  irImportacion(row)
  {
    this.procesoSelected = row;
    this.tabSelected.setValue(3);
    setTimeout(()=>this.tabPadron.obtenerEmpadronados());
    this.tabImportarPadronDesabilitado = false;
  }

  irTabCandidatos(row){
    this.tabSelected.setValue(2); // Indice del Tab para candidatos
    this.listaSelected = row;
    this.tabCandidatosDesabilitado = false;
    this.candidatoComponent.limpiarArregloCandidatos();
    setTimeout(()=> {
      this.candidatoComponent.getItems();
      this.candidatoComponent.getEscaniosDisponibles();

    });
  }



}
