import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {CrudService} from '../../../../../shared/services/facade-services/crud.service';
import {UtilService} from '../../../../../shared/services/util/util.service';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {Candidato, Escanio} from '../../../../constantes/apis';

import {Auditoria} from '../../../../constantes/variables-globales';
import {ApiResponseMessageModel} from '../../../../../shared/models/voto-electronico/api-response-message.model';
import {PopupCandidatoComponent} from './popup-candidato/popup-candidato.component';
import {AppConfirmService} from '../../../../../shared/services/app-confirm/app-confirm.service';
import {AppLoaderService} from '../../../../../shared/services/app-loader/app-loader.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
    selector: 'app-tab-candidatos',
    templateUrl: './tab-candidatos.component.html',
    styleUrls: ['./tab-candidatos.component.less']
})
export class TabCandidatosComponent implements OnInit, OnDestroy {
    @Input() listaSelected;
    @Input() procesoSelected;
    listaCandidatos;
    itemForm: FormGroup;
    listaEscanios;
    esAlterno:boolean = false;
    private unsuscribe$ = new Subject<void>(); //marca como completado el observable
    constructor(
        private fb: FormBuilder,
        private crudService: CrudService,
        private util: UtilService,
        private dialog: MatDialog,
        private confirmService: AppConfirmService,
        private loader: AppLoaderService
    ) {
        //this.getEscaniosDisponibles();
    }

    ngOnInit(): void {
        this.buildItemForm();
    }
    ngOnDestroy() {
        this.unsuscribe$.next();
        this.unsuscribe$.complete();
    }

    buildItemForm() {
        this.itemForm = this.fb.group({
            procesoElectoralId: [''],
            listaId: [''],
            escanioId: [''],
            personaId: [''],
            estado: ['']
        })
    }

    getItems(noLoader?) {
        if(!noLoader)this.loader.open()
        const data = this.itemForm.value;
        this.crudService.post({
            listaId: this.listaSelected.id,
            estado: Auditoria.ACTIVO,

        }, Candidato.ObtenerCandidatoMedianteListaId)
            .pipe(
                takeUntil(this.unsuscribe$)
            ).subscribe(
            (result: ApiResponseMessageModel) => {
                if (!this.util.existenErrores(result)) {
                    if (result.responseList) {
                        this.listaCandidatos = this.util.obtenerListaMensaje(result);

                    } else {
                        this.util.mostrarMensaje(result)
                        this.listaCandidatos = [];
                    }
                }
                if(!noLoader)this.loader.close()
            },
            ()=>
            {
                if(!noLoader)this.loader.close()
            }
        )
    }

    getEscaniosDisponibles() {
        this.crudService.post({
            Id: this.listaSelected.procesoElectoralId
        }, Escanio.ObtenerEscaniosDisponiblesMedianteProcesoElectoralId)
            .pipe(
                takeUntil(this.unsuscribe$)
            )
            .subscribe(response => {
                if (!this.util.existenErrores(response)) {
                    if (response.responseList) {
                        this.listaEscanios = this.util.obtenerListaMensaje(response)
                    } else {
                        this.util.mostrarMensaje(response)
                    }
                }
            })
    }


    openPoup(data: any = {}, isNew?: boolean) {
        let title = isNew ? 'Nuevo candidato' : 'Actualizar candidato';
        let listaId = this.listaSelected.id;
        let dialogRef: MatDialogRef<any> = this.dialog.open(
            PopupCandidatoComponent,
            {
                width: this.procesoSelected.aplicaAlterno ? '1000px': '500px',
                disableClose: true,
                data: {title: title, payload: data, listaId: listaId, aplicaAlterno: this.procesoSelected.aplicaAlterno}
            }
        );
        dialogRef.afterClosed().subscribe(res => {
            if (!res) {
                // If user press cancel
                return;
            }
            this.loader.open();
            const consulta = isNew ? this.crudService.post({
                    personaId: res.persona.id,
                    personaIdAlterno: res.personaAlterno.id,
                    escanioId: res.escanioId,
                    listaId: this.listaSelected.id,
                    estado: Auditoria.ACTIVO,
                    fotoObjeto: res.imagenCandidato,
                    fotoObjetoAlterno: res.imagenCandidatoAlterno
                }, Candidato.CrearCandidato) :
                this.crudService.put({
                    id: data.id,
                    personaId: res.persona.id,
                    escanioId: res.escanioId,
                    listaId: this.listaSelected.id,
                    fotoObjeto: res.imagenCandidato,
                    estado: Auditoria.ACTIVO
                }, Candidato.ActualizarCandidato);

            consulta
                .pipe(
                    takeUntil(this.unsuscribe$)
                ).subscribe(
                (resp) => {
                    if (!this.util.existenErrores(resp)) {
                        this.util.mostrarMensaje(resp);
                        this.getItems(true);
                    }
                    this.loader.close();
                },
                ()=>
                {
                    this.loader.close();
                }
            )
        });

    }


    deleteItem(candidato) {

        this.confirmService.confirm({
            title: 'Confirmar',
            message: 'Se eliminará el candidato seleccionado. ¿Desea de continuar?'
        })
            .subscribe(respuestaOK => {
                if (respuestaOK) {
                    this.servicioEliminarCandidato(candidato);
                }
            })


    }


    servicioEliminarCandidato(candidato) {
        this.loader.open();
        this.crudService.delete({
            idCandidato: candidato.id,
        }, Candidato.EliminarCandidato)
            .pipe(
                takeUntil(this.unsuscribe$)
            ).subscribe(
            (resp) => {
                this.loader.close();
                this.util.mostrarMensaje(resp);
                this.getItems(true);
            },
            ()=>
            {
             this.loader.close()
            }
        )
    }

    limpiarArregloCandidatos() {
        this.listaCandidatos = [];
    }
}
