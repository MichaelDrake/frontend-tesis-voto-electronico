import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {CrudService} from '../../../../../../shared/services/facade-services/crud.service';
import {UtilService} from '../../../../../../shared/services/util/util.service';
import {Eleccion, Escanio} from '../../../../../constantes/apis';
import {Ng2ImgMaxService} from 'ng2-img-max';
import {ApiResponseMessageModel} from '../../../../../../shared/models/voto-electronico/api-response-message.model';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
    selector: 'app-popup-candidato',
    templateUrl: './popup-candidato.component.html',
    styleUrls: ['../tab-candidatos.component.less']
})
export class PopupCandidatoComponent implements OnInit,OnDestroy {

    listaEscaniosDisponibles = [];
    itemForm: FormGroup;
    deshabilitarNuevo = false;
    private unsuscribe$ = new Subject<void>(); //marca como completado el observable
    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        public dialogRef: MatDialogRef<PopupCandidatoComponent>,
        private fb: FormBuilder,
        private crudService: CrudService,
        private util: UtilService,
        private ng2ImgMaxService: Ng2ImgMaxService,
    ) {
    }

    ngOnInit(): void {
        this.buildItemForm(this.data.payload);
        this.getEscaniosDisponibles();
    }
    ngOnDestroy() {
        this.unsuscribe$.next();
        this.unsuscribe$.complete();
    }

    buildItemForm(data: any = {}) {
        this.itemForm = this.fb.group({
            imagenCandidato: [data.fotoUrl || '', Validators.required],
            persona: new FormControl(data.identificacion || '', [Validators.required]),
            imagenCandidatoAlterno: [data.fotoUrlAlterno || '', this.data.aplicaAlterno ? Validators.required : null ],
            personaAlterno: [data.identificacionAlterno || '', this.data.aplicaAlterno ? Validators.required : null ],
            escanioId: [data.escanioId || '', Validators.required],
            identificacion: new FormControl({value:'', disabled: true},[]),
            identificacionAlterno: new FormControl({value:'', disabled: true},[])
        });
    }


    getEscaniosDisponibles() {
        this.crudService.post({
            Id: this.data.listaId,
        }, Escanio.ObtenerEscaniosDisponiblesMedianteListaId)
            .pipe(
                takeUntil(this.unsuscribe$)
            ).subscribe(
            (result:ApiResponseMessageModel) => {
                if (!this.util.existenErrores(result)) {
                    if(result.responseList){
                        this.listaEscaniosDisponibles = this.util.obtenerListaMensaje(result,false)
                        this.deshabilitarNuevo = false
                    }
                    else{
                        this.util.mostrarMensaje(result)
                        this.deshabilitarNuevo = true
                    }

                }
            }
        )
    }

    /**
     * maneja las imagenes que se cargan
     * @param event
     */
    onSelectFile(event) {

        //console.log(event)
        const extensionImg = event.target.files[0];
        // if (!this.isNew) {
        //     this.listaImagenes = [];
        // }
        if (event.target.files && event.target.files[0]) {
            // this.cargandoImagen = true
            let file = event.target.files[0];

            this.ng2ImgMaxService.resizeImage(file, 720
                , 450).subscribe(result => {
                var reader = new FileReader();

                reader.onload = (event: any) => {
                    const logo = {
                        base64: event.target.result,
                        tipo: extensionImg.type,
                        extension: '.' + extensionImg.name.split('.')[1],
                    };
                    // this.cargandoImagen = false;
                    this.itemForm.controls['imagenCandidato'].setValue(logo);
                    this.itemForm.controls['imagenCandidato'].updateValueAndValidity();
                };
                reader.readAsDataURL(result);

            })
        }
    }
    onSelectFileAlterno(event)
    {
        const extensionImg = event.target.files[0];
        if (event.target.files && event.target.files[0]) {
            let file = event.target.files[0];
            this.ng2ImgMaxService.resizeImage(file, 720
                , 450).subscribe(result => {
                var reader = new FileReader();

                reader.onload = (event: any) => {
                    const logo = {
                        base64: event.target.result,
                        tipo: extensionImg.type,
                        extension: '.' + extensionImg.name.split('.')[1],
                    };
                    this.itemForm.controls['imagenCandidatoAlterno'].setValue(logo);
                    this.itemForm.controls['imagenCandidatoAlterno'].updateValueAndValidity();
                };
                reader.readAsDataURL(result);
            })
        }
    }
}
