import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {CrudService} from '../../../../../shared/services/facade-services/crud.service';
import {UtilService} from '../../../../../shared/services/util/util.service';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {ProcesoElectoral} from '../../../../constantes/apis';
import {PopupProcesoElectoralComponent} from './popup-proceso-electoral/popup-proceso-electoral.component';
import {DatePipe} from '@angular/common';
import {Auditoria, OpcionesBusquedaProcesos} from '../../../../constantes/variables-globales';
import {ApiResponseMessageModel} from '../../../../../shared/models/voto-electronico/api-response-message.model';
import {AppConfirmService} from '../../../../../shared/services/app-confirm/app-confirm.service';
import {AppLoaderService} from '../../../../../shared/services/app-loader/app-loader.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
    selector: 'app-proceso-electoral',
    templateUrl: './proceso-electoral.component.html',
    styleUrls: ['./proceso-electoral.component.less']
})
export class ProcesoElectoralComponent implements OnInit, OnDestroy {

    @Output() procesoSelected = new EventEmitter<any>();
    @Output() irImportacion = new EventEmitter<any>();

    itemForm: FormGroup;
    procesosElectorales: [];
    opcionesDeBusqueda = [];
    parametroDeBusquedaSeleccionado = '';
    matChip = {
        backgroundColor: '',
        label: '',
    };
    today = new Date();
    private unsuscribe$ = new Subject<void>(); //marca como completado el observable
    constructor(
        private fb: FormBuilder,
        private crudService: CrudService,
        private util: UtilService,
        private dialog: MatDialog,
        private datePipe: DatePipe,
        private confirmService: AppConfirmService,
        private loader: AppLoaderService,
    ) {
    }


    ngOnInit(): void {
        this.buildItemForm();
        this.opcionesDeBusqueda = OpcionesBusquedaProcesos
        this.getItems(false);
    }
    ngOnDestroy() {
        this.unsuscribe$.next();
        this.unsuscribe$.complete();
    }

    buildItemForm() {
        this.itemForm = this.fb.group({
            nombreProcesoElectoral: [''],
            estado: [Auditoria.ACTIVO],
            anioSeleccionado: [''],
        })
    }

    getItems(noLoader?) {
        this.procesosElectorales= []
        if(!noLoader)this.loader.open()
        const data = this.itemForm.value;

        if (!data.estado) {
            data.estado = Auditoria.ACTIVO
        }
        this.crudService.post({
            parametroBusqueda1: data.nombreProcesoElectoral,
            parametroBusqueda2: data.estado,
            parametroBusqueda4: data.anioSeleccionado,
        }, ProcesoElectoral.ObtenerProcesosElectoralesByParametros)
            .pipe(
                takeUntil(this.unsuscribe$)
            )
            .subscribe(
            (result: ApiResponseMessageModel) => {
                    if (result.responseList != null) {
                        this.procesosElectorales = this.util.obtenerListaMensaje(result)
                    } else {
                        this.util.mostrarMensaje(result)
                    }
                if(!noLoader)this.loader.close();
            },
            () => {
                if(!noLoader)this.loader.close();
            }
        )
    }

    openPoup(data: any = {}, isNew?: boolean) {
        let title = isNew ? 'Nuevo proceso electoral' : 'Actualizar proceso electoral ';
        let dialogRef: MatDialogRef<any> = this.dialog.open(
            PopupProcesoElectoralComponent,
            {
                width: '600px',
                disableClose: true,
                data: {title: title, payload: data}
            }
        );
        dialogRef.afterClosed().subscribe(res => {

            if (!res) {
                // If user press cancel
                return;
            }
            this.loader.open();

            var fechaI = new Date(res.diaEleccion);
            var fechaF = new Date(res.diaEleccion);

            var fechaInicio = fechaI
            fechaInicio.setHours(res.horaInicio.getHours())
            fechaInicio.setMinutes(res.horaInicio.getMinutes())
            fechaInicio.setSeconds(0)

            var fechaFin = fechaF
            fechaFin.setHours(res.horaFin.getHours())
            fechaFin.setMinutes(res.horaFin.getMinutes())
            fechaFin.setSeconds(0)

            var dto = {
                nombreProcesoElectoral: res.nombreProcesoElectoral,
                fechaInicio: this.datePipe.transform(fechaInicio, 'yyyy-MM-dd hh:mm:ss a'),
                fechaFin: this.datePipe.transform(fechaFin, 'yyyy-MM-dd hh:mm:ss a'),
                //fechaFin: fechaFin,
                eleccionId: res.eleccionId,
                estado: Auditoria.ACTIVO
            }
            const consulta = isNew ? this.crudService.post(dto, ProcesoElectoral.CrearProcesoElectoral) :
                this.crudService.put({
                    id: data.id,
                    nombreProcesoElectoral: res.nombreProcesoElectoral,
                    fechaInicio: this.datePipe.transform(fechaInicio, 'yyyy-MM-dd hh:mm:ss a'),
                    fechaFin: this.datePipe.transform(fechaFin, 'yyyy-MM-dd hh:mm:ss a'),
                    eleccionId: res.eleccionId,
                    estado: Auditoria.ACTIVO
                }, ProcesoElectoral.ActualizarProcesoElectoral);

            consulta
                .pipe(
                takeUntil(this.unsuscribe$)
            ).subscribe(
                (resp: ApiResponseMessageModel) => {
                    if (!this.util.existenErrores(resp)) {
                        this.util.mostrarMensaje(resp);
                        this.getItems(true);
                    }
                    this.loader.close();
                },
                ()=>
                {
                    this.loader.close();
                }
            )

        });

    }


    deleteItem(row) {

        this.confirmService.confirm({
            title: 'Confirmar',
            message: 'Se eliminará el proceso electoral seleccionado y todas las configuraciones de listas y candidatos. ¿Desea continuar?'
        })
            .subscribe(respuestaOK => {
                if (respuestaOK) {
                    this.servicioEliminarProcesoElectoral(row);
                }
            })

    }


    servicioEliminarProcesoElectoral(row) {
        this.loader.open();
        this.crudService.delete({
            idProcesoElectoral: row.id
        }, ProcesoElectoral.EliminarProcesoElectoral)
            .pipe(
                takeUntil(this.unsuscribe$)
            ).subscribe(
            (resp) => {

                if (!this.util.existenErrores(resp)) {
                    this.util.mostrarMensaje(resp);
                    this.getItems(true);
                }
                this.loader.close();
            },
            (resp)=>
            {

                //this.util.existenErrores(resp.error)
                this.loader.close();
            }
        )
    }

    habilitarItem(row) {
        this.crudService.put({
            id: row.id,
            nombreProcesoElectoral: row.nombreProcesoElectoral,
            fechaInicio: row.fechaInicio,
            fechaFin: row.fechaFin,
            eleccionId: row.eleccionId,
            estado: Auditoria.ACTIVO
        }, ProcesoElectoral.EliminarProcesoElectoral).
            pipe(
                takeUntil(this.unsuscribe$)
            ).
        subscribe(
            (resp) => {
                this.util.mostrarMensaje(resp);
                this.getItems();
            }
        )
    }

    irLista(row) {
        this.procesoSelected.emit(row)
    }


    limpiar() {
        this.parametroDeBusquedaSeleccionado = '';
        this.itemForm.reset();
    }

}
