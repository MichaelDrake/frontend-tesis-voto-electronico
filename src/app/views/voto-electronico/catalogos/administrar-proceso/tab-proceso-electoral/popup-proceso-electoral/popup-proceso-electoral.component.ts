import {Component, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {CrudService} from '../../../../../../shared/services/facade-services/crud.service';
import {Eleccion, TipoEleccion} from '../../../../../constantes/apis';
import {UtilService} from '../../../../../../shared/services/util/util.service';
import {Auditoria, horas} from '../../../../../constantes/variables-globales';
import {CustomValidators} from 'ngx-custom-validators';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
    selector: 'app-popup-proceso-electoral',
    templateUrl: './popup-proceso-electoral.component.html',
    styleUrls: ['../proceso-electoral.component.less']
})
export class PopupProcesoElectoralComponent implements OnInit, OnDestroy {

    lsElecciones = [];
    itemForm: FormGroup;
    diaInicioMin; //Obtener un dia mas que el actual
    horaInicioMin ; // Hora de inicio minimo para una eleccion
    horaFinMax ;  // hora de fin maxximo para una eleccion
    horaInicioActual; // Empieza en vacio y se llema con 'horaInicioMin'
    private unsuscribe$ = new Subject<void>(); //marca como completado el observable
    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        public dialogRef: MatDialogRef<PopupProcesoElectoralComponent>,
        private fb: FormBuilder,
        private crudService: CrudService,
        private util: UtilService,
    ) {
    }

    ngOnInit(): void {
        this.getElecciones();
        this.establecerFechaUnDiaDespues();
        this.establercerHoraDeInicio();
        this.establecerHoraDeFin();

        this.buildItemForm(this.data.payload);
    }
    ngOnDestroy() {
        this.unsuscribe$.next();
        this.unsuscribe$.complete();
    }


    buildItemForm(data: any = {}) {
        this.itemForm = this.fb.group({
            nombreProcesoElectoral: new FormControl(data.nombreProcesoElectoral ||'' , [
                Validators.required,
                Validators.maxLength(199),
            ]),
            diaEleccion: [new Date(data.fechaInicio) || '', Validators.required],
            horaInicio: [ data.fechaInicio ? new Date(data.fechaInicio) : new Date(this.horaInicioMin) , Validators.required],
            horaFin: [data.fechaFin ? new Date(data.fechaFin) : new Date(this.horaFinMax), Validators.required],
            eleccionId: [data.eleccionId || '', Validators.required],
        })
    }


    getElecciones() {
        this.crudService.post({
            nombreEleccion: null,
            estado: Auditoria.ACTIVO
        }, Eleccion.ObtenerEleccionByParametros)
            .pipe(
                takeUntil(this.unsuscribe$)
            )
            .subscribe(
            (result) => {
                if (!this.util.existenErrores(result)) {
                    this.lsElecciones = this.util.obtenerListaMensaje(result)
                }
            }
        )
    }

    establecerFechaUnDiaDespues() {
        const fechaHoy = new Date();
        const fechaManiana = new Date();
        fechaManiana.setDate(fechaHoy.getDate() + 1);
        fechaManiana.setHours(0)
        fechaManiana.setMinutes(0)
        fechaManiana.setSeconds(0)
        this.diaInicioMin = fechaManiana

    }

    establercerHoraDeInicio(){
        let fechaInicio;

        if(this.data.payload.fechaInicio != null){
            fechaInicio = new Date(this.data.payload.fechaInicio);
        }else{

            fechaInicio = new Date();
        }
        fechaInicio.setHours(horas.horaInicio)
        fechaInicio.setMinutes(0)
        fechaInicio.setSeconds(0)
        this.horaInicioMin = fechaInicio

    }


    establecerHoraDeFin(){
        let fechaFin;
        if(this.data.payload.fechaFin){
            fechaFin = new Date(this.data.payload.fechaFin);
        }
        else{
            fechaFin = new Date();
        }

        fechaFin.setHours(horas.horaFin)
        fechaFin.setMinutes(0);
        fechaFin.setSeconds(0)
        this.horaFinMax = fechaFin

    }

    obtenerHoraMinimaSeleccionada(event) {
        this.horaInicioActual = event.value;
    }

    public omitirFinDeSemana = (d: Date): boolean => {
        const day = d.getDay();
        return day !== 0 && day !== 6;
    }

    prueba(event){


    }
}
