import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {CrudService} from '../../../../../../shared/services/facade-services/crud.service';
import {UtilService} from '../../../../../../shared/services/util/util.service';
import {Ng2ImgMaxService} from 'ng2-img-max';
import {AppLoaderService} from '../../../../../../shared/services/app-loader/app-loader.service';
import {Padron, Usuario} from '../../../../../constantes/apis';
import {ApiResponseMessageModel} from '../../../../../../shared/models/voto-electronico/api-response-message.model';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-popup-empadronado',
  templateUrl: './popup-empadronado.component.html',
  styleUrls: ['../tab-importacion-padron.component.less']
})
export class PopupEmpadronadoComponent implements OnInit, OnDestroy {
  formData = {}
  itemForm: FormGroup;
  title='';
  listaRoles;
  private unsuscribe$ = new Subject<void>(); //marca como completado el observable
  constructor(
      @Inject(MAT_DIALOG_DATA) public data: any,
      public dialogRef: MatDialogRef<PopupEmpadronadoComponent>,
      private fb: FormBuilder,
      private crudService: CrudService,
      private util: UtilService,
      private ng2ImgMaxService: Ng2ImgMaxService,
      private loader: AppLoaderService,
  ) { }

  ngOnInit(): void {
    this.title = this.data.isNew ? 'Añadir empadronado' : 'Actualizar empadronado';
    this.buildItemForm(this.data.payload);
  }
  ngOnDestroy() {
    this.unsuscribe$.next();
    this.unsuscribe$.complete();
  }

  buildItemForm(data: any = {}){

    this.itemForm = this.fb.group({
      usuario: new FormControl(data || '' , [
        Validators.required,
      ]),
      identificacion:[data.nombreUsuario || '']
    })

    this.itemForm.controls['identificacion'].disable();

  }


  crearEmpadronado(){
    this.loader.open();

    if (this.itemForm.value['usuario'].dt1 == null){
      this.loader.close();
      return;
    }
    else{
      let dto = {
        procesoElectoralId : this.data.procesoElectoralId,
        usuarioId : this.itemForm.value['usuario'].id,
        votoRealizado: false,
        identificacionEmpadronado: this.itemForm.value['usuario'].dt1
      }

      const consulta = this.crudService.post(dto, Padron.AñadirEmpadronado)

      consulta.pipe(
          takeUntil(this.unsuscribe$)
      )
          .subscribe(
          (response: ApiResponseMessageModel) => {
            if (!this.util.existenErrores(response)) {
              this.util.mostrarMensaje(response);
              if(response.responseList != null && response.responseList.length > 0){
                this.dialogRef.close(response);
              }
            }

            this.loader.close();
          },()=>{
            this.loader.close();
          }
      )
    }


  }

}
