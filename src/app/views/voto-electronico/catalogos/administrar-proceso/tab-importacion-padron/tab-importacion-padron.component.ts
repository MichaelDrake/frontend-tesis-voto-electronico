import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Padron} from '../../../../constantes/apis';
import {CrudService} from '../../../../../shared/services/facade-services/crud.service';
import {AppLoaderService} from '../../../../../shared/services/app-loader/app-loader.service';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {AppConfirmService} from '../../../../../shared/services/app-confirm/app-confirm.service';
import {UtilService} from '../../../../../shared/services/util/util.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {PopupErrorMensajeComponent} from './popup-error/popup-error.component';
import {ApiResponseMessageModel} from '../../../../../shared/models/voto-electronico/api-response-message.model';
import {PopupEmpadronadoComponent} from './popup-empadronado/popup-empadronado.component';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
    selector: 'app-tab-importacion-padron',
    templateUrl: './tab-importacion-padron.component.html',
    styleUrls: ['./tab-importacion-padron.component.less']
})
export class TabImportacionPadronComponent implements OnInit, OnDestroy {
    @Input() procesoSelected?: any;
    lstPadron: any[] = [];
    itemForm: FormGroup
    paginaSeleccionada = 0;
    totalRegistros;
    private unsuscribe$ = new Subject<void>(); //marca como completado el observable
    constructor(private crudService: CrudService,
                private loader: AppLoaderService,
                private dialog: MatDialog,
                private confirmService: AppConfirmService,
                private util: UtilService,
                private fb: FormBuilder) {
    }

    ngOnInit(): void {
        this.buildItemForm();
    }
    buscar()
    {
        this.paginaSeleccionada= 0;
        this.totalRegistros = 0;
        this.lstPadron = [];
        this.getItems();
    }

    getItems(noLoader?) {
        if(!noLoader)this.loader.open()
        this.crudService.post({
            id: this.procesoSelected?.id,
            nombreEmpadronado: this.itemForm.value.nombre,
            pagina: this.paginaSeleccionada
        }, Padron.ObtenerPadronesNombre)
            .pipe(
                takeUntil(this.unsuscribe$)
            )
            .subscribe((result:ApiResponseMessageModel) => {
                if(result.responseList != null){
                    this.lstPadron = this.util.obtenerListaMensaje(result);
                    this.totalRegistros = this.lstPadron.length > 0 ? this.lstPadron[0].total : 0;
                    if(this.totalRegistros == 0)
                        this.util.mostrarMensaje({
                        informationMessage: {
                            descripcion : "No se encontraron registros de empadronados",
                            tipoMensajeId: "AVISO",
                        }
                    })
                    if(!noLoader)setTimeout(() => this.loader.close())
                }else{
                    this.util.mostrarMensaje(result);
                }

                },
                () => {
                    if(!noLoader)setTimeout(() => this.loader.close())
                });
    }

    buildItemForm() {
        this.itemForm = this.fb.group({
            nombre: []
        })
    }

    obtenerEmpadronados() {
        this.paginaSeleccionada= 0;
        this.totalRegistros = 0;
        this.lstPadron= [];
        this.getItems();
    }


    limpiar() {
        this.lstPadron = [];
    }
    limpiarBusqueda()
    {
        this.itemForm.reset();
    }

    onSelectFile(e: any) {
        let formdata = new FormData();
        const dto = {
            id: this.procesoSelected?.id
        };
        formdata.append('myfile', e.target.files[0]);
        this.loader.open('Cargando Información');
        this.crudService.archivo(formdata, dto, Padron.ImportarPadron)
            .pipe(
                takeUntil(this.unsuscribe$)
            )
            .subscribe(data => {
            this.getItems(true);
            this.loader.close();
            const cabecera = JSON.parse(data.headers.get('dto'));
            this.loader.close();
            if (cabecera.bdt1) {
                this.openDialog(cabecera.dt1, cabecera.ndt1, cabecera.ndt2, cabecera.ndt3);
            } else {
                this.openDialog(cabecera.dt1);
            }
        }, error => {
            this.loader.close();
            //this.openDialog('Error, no se ha podido importar el archivo correctamente');
        })
    }

    openDialog(mensajeError, total: number = 0, registrados: number = 0, errores: number = 0): void {
        let alternativo: string = '';
        if (errores > 0) {
            mensajeError = 'Registros totales: ' + total.toString();
            alternativo = 'registros ingresados: ' + registrados.toString() + ', registros con error: ' + errores.toString();
        }
        this.dialog.open(PopupErrorMensajeComponent, {
            width: '400px',
            data: {mensaje: mensajeError, mensajeAlternativo: alternativo, cantidadErrores: errores, cantidadExitos: registrados}
        });
    }


    validarImportacion(file) {
        this.confirmService.confirm({
            title: 'Confirmar',
            message: 'Si continúa con la importación se sobreescribirán los datos previamente cargados, ¿Desea Continuar?'
        })
            .subscribe(v => {
                if (v) {
                    file.click();
                }
            })

    }

    openPoup(data: any = {}, isNew?: boolean) {

        let dialogRef: MatDialogRef<any> = this.dialog.open(
            PopupEmpadronadoComponent,
            {
                width: '500px',
                disableClose: true,
                data: {payload: data, isNew: isNew, procesoElectoralId: this.procesoSelected.id}
            }
        );
        dialogRef.afterClosed().subscribe(res => {

            if (!res) {
                // If user press cancel
                return;
            }

            this.getItems();

        });

    }

    deleteItem(row) {

        this.confirmService.confirm({
            title: 'Confirmar',
            message: 'Se eliminará el registro seleccionado. ¿Desea continuar?'
        })
            .subscribe(respuestaOK => {
                if (respuestaOK) {
                    this.servicioEliminarProcesoElectoral(row);
                }
            })

    }

    servicioEliminarProcesoElectoral(row) {
        this.loader.open();
        this.crudService.post({
            procesoElectoralId: this.procesoSelected.id,
            id: row.id
        }, Padron.EliminarRegistro)
            .pipe(
                takeUntil(this.unsuscribe$)
            )
            .subscribe(
            (resp) => {
                if (!this.util.existenErrores(resp)) {
                    this.util.mostrarMensaje(resp);
                    this.getItems(true);
                }
                this.loader.close();
            },
            ()=>
            {
                this.loader.close();
            }
        )
    }
    setPage(event)
    {
        this.lstPadron = [];
        this.paginaSeleccionada = event.offset;
        this.getItems()
    }
    ngOnDestroy() {
        this.unsuscribe$.next();
        this.unsuscribe$.complete();
    }
}
