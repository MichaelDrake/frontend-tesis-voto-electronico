import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Subject} from 'rxjs';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {UtilService} from '../../../../../../shared/services/util/util.service';

@Component({
  selector: 'app-popup-error',
  templateUrl: './popup-error.component.html',
  styles: [
  ]
})
export class PopupErrorMensajeComponent implements OnInit {
  itemForm: FormGroup;
  mensajeError: string;
  mensajeErrorAlternativo: string = "";

  titulo: string = "No se han podido importar los archivos seleccionados";

  private unsuscribe$ = new Subject<void>(); //marca como completado el observable

  constructor(
      @Inject(MAT_DIALOG_DATA) public data: any,
      public dialogRef: MatDialogRef<PopupErrorMensajeComponent>,
      private fb: FormBuilder,
      public util: UtilService,
  ) {}

  ngOnInit() {
    this.buildItemForm(this.data);
    this.mensajeError = this.data.mensaje;
    this.mensajeErrorAlternativo = this.data.mensajeAlternativo;

    if(this.data.cantidadErrores == 0 && this.data.cantidadExitos > 0){
      this.titulo = "Registro exitoso";
    }
    if(this.data.cantidadErrores > 0 && this.data.cantidadExitos > 0){
      this.titulo = "Importación con errores";
    }
    if(this.data.cantidadErrores == 0 && this.data.cantidadExitos == 0){
      this.titulo = "";
    }
  }

  /***
   * Elimina todas las subscribciones
   * de los observables
   */
  ngOnDestroy(): void {
    this.unsuscribe$.next(); // enviar el siguiente dato
    this.unsuscribe$.complete(); // marca como completado al observable
  }

  /**
   *Construcción de formulario
   *
   * @param {*} item
   * @memberof ConfiguracionNodoPopupComponent
   */
  buildItemForm(item) {
    this.itemForm = this.fb.group({});
  }
}
