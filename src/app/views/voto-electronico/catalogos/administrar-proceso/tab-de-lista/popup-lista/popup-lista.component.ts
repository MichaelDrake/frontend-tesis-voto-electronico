import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {CrudService} from '../../../../../../shared/services/facade-services/crud.service';
import {UtilService} from '../../../../../../shared/services/util/util.service';
import {Eleccion, ProcesoElectoral} from '../../../../../constantes/apis';
import {FileUploader} from 'ng2-file-upload';
import {Ng2ImgMaxService} from 'ng2-img-max';
import {Auditoria} from '../../../../../constantes/variables-globales';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
    selector: 'app-popup-lista',
    templateUrl: './popup-lista.component.html',
    styleUrls: ['../tab-de-lista.component.less']
})
export class PopupListaComponent implements OnInit, OnDestroy{

    lsProcesosElectorales = [];
    itemForm: FormGroup;
    private unsuscribe$ = new Subject<void>(); //marca como completado el observable
    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        public dialogRef: MatDialogRef<PopupListaComponent>,
        private fb: FormBuilder,
        private crudService: CrudService,
        private util: UtilService,
        private ng2ImgMaxService: Ng2ImgMaxService,
    ) {
    }

    ngOnInit(): void {
        this.buildItemForm(this.data.payload);
        this.getProcesosElectorales();
    }
    ngOnDestroy() {
        this.unsuscribe$.next();
        this.unsuscribe$.complete();
    }


    buildItemForm(data: any = {}) {
        this.itemForm = this.fb.group({
            nombreLista: [data.nombreLista || '', Validators.required],
            eslogan: [data.eslogan || ''],
            logoLista: [data.logoUrl || '', Validators.required],
        })


    }


    getProcesosElectorales() {
        this.crudService.post({
            nombreProcesoElectoral: null,
            estado: Auditoria.ACTIVO
        }, ProcesoElectoral.ObtenerProcesosElectoralesByParametros).pipe(
            takeUntil(this.unsuscribe$)
        ).subscribe(
            (result) => {
                if (!this.util.existenErrores(result)) {
                    this.lsProcesosElectorales = this.util.obtenerListaMensaje(result)

                }
            }
        )
    }

    /**
     * maneja las imagenes que se cargan
     * @param event
     */
    onSelectFile(event) {
        //console.log(event)
        const extensionImg = event.target.files[0];
        // if (!this.isNew) {
        //     this.listaImagenes = [];
        // }
        if (event.target.files && event.target.files[0]) {
            // this.cargandoImagen = true
            let file = event.target.files[0];

            this.ng2ImgMaxService.resizeImage(file, 720
                , 450).subscribe(result => {
                var reader = new FileReader();

                reader.onload = (event: any) => {
                    const logo = {
                        base64: event.target.result,
                        tipo: extensionImg.type,
                        extension: '.' + extensionImg.name.split('.')[1],
                    };
                    // this.cargandoImagen = false;
                    this.itemForm.controls['logoLista'].setValue(logo);
                    this.itemForm.controls['logoLista'].updateValueAndValidity();
                };
                reader.readAsDataURL(result);

            })

            // for (let file of filesList) {
            //     this.ng2ImgMaxService.resizeImage(file, 720, 450).subscribe(result => {
            //         var reader = new FileReader();
            //         reader.onload = (event: any) => {
            //             this.listaImagenes.push({
            //                 dt1: event.target.result,
            //                 dt2: extensionImg.type,
            //                 dt3: '.' + extensionImg.name.split('.')[1],
            //             });
            //             // this.cargandoImagen = false;
            //         };
            //         reader.readAsDataURL(result);
            //     })
            //
            // }

        }
    }


}
