import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {CrudService} from '../../../../../shared/services/facade-services/crud.service';
import {UtilService} from '../../../../../shared/services/util/util.service';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {Lista} from '../../../../constantes/apis';
import {PopupListaComponent} from './popup-lista/popup-lista.component';
import {Auditoria} from '../../../../constantes/variables-globales';
import {ApiResponseMessageModel} from '../../../../../shared/models/voto-electronico/api-response-message.model';
import {AppConfirmService} from '../../../../../shared/services/app-confirm/app-confirm.service';
import {AppLoaderService} from '../../../../../shared/services/app-loader/app-loader.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
    selector: 'app-tab-de-lista',
    templateUrl: './tab-de-lista.component.html',
    styleUrls: ['./tab-de-lista.component.less']
})
export class TabDeListaComponent implements OnInit, OnDestroy {
    @Input() procesoSelected?;
    @Output() listaSelected = new EventEmitter<any>();
    itemForm: FormGroup;
    listas: []
    private unsuscribe$ = new Subject<void>(); //marca como completado el observable
    constructor(
        private fb: FormBuilder,
        private crudService: CrudService,
        private util: UtilService,
        private dialog: MatDialog,
        private confirmService: AppConfirmService,
        private loader: AppLoaderService
    ) {
    }

    ngOnInit(): void {
        this.buildItemForm();
    }
    ngOnDestroy() {
        this.unsuscribe$.next();
        this.unsuscribe$.complete();
    }

    buildItemForm() {
        this.itemForm = this.fb.group({
            nombreLista: [''],
            estado: [Auditoria.ACTIVO],
        })
    }

    getItems(noLoader?) {
        if(!noLoader)this.loader.open();
        this.crudService.post({
            procesoElectoralId: this.procesoSelected.id,
            estado: Auditoria.ACTIVO
        }, Lista.ObtenerListasPorProcesoElectoralId)
            .pipe(
                takeUntil(this.unsuscribe$)
            ).subscribe(
            (result: ApiResponseMessageModel) => {

                if (!this.util.existenErrores(result)) {
                    if(result.responseList){
                        this.listas = this.util.obtenerListaMensaje(result).map(
                            (lista: any) => {
                                lista.bagde = lista.estado == Auditoria.ACTIVO ? true : false;
                                return lista;
                            }
                        );
                    }
                    else{
                        this.util.mostrarMensaje(result)
                    }
                }
                if(!noLoader)this.loader.close();
            },
            ()=>
            {
                if(!noLoader)this.loader.close();
            }
        )
    }

    openPoup(data: any = {}, isNew?: boolean) {
        let title = isNew ? 'Nueva Lista' : 'Actualizar lista ';
        let dialogRef: MatDialogRef<any> = this.dialog.open(
            PopupListaComponent,
            {
                width: '500px',
                disableClose: true,
                data: {title: title, payload: data}
            }
        );
        dialogRef.afterClosed().subscribe(res => {

            if (!res) {
                // If user press cancel
                return;
            }
            this.loader.open();
            const consulta = isNew ? this.crudService.post({
                    nombreLista: res.nombreLista,
                    procesoElectoralId: this.procesoSelected.id,
                    eslogan: res.eslogan,
                    logoObjeto: res.logoLista,
                    estado: Auditoria.ACTIVO
                }, Lista.CrearLista) :
                this.crudService.put({
                    id: data.id,
                    nombreLista: res.nombreLista,
                    procesoElectoralId: this.procesoSelected.id,
                    eslogan: res.eslogan,
                    logoObjeto: res.logoLista,
                    estado: Auditoria.ACTIVO
                }, Lista.ActualizarLista);

            consulta
                .pipe(
                    takeUntil(this.unsuscribe$)
                ).subscribe(
                (resp) => {
                    this.loader.close()
                    this.util.mostrarMensaje(resp);
                    this.getItems(true);
                },
                ()=>
                {
                    this.loader.close();
                }
            )
        });

    }


    deleteItem(row) {
        this.confirmService.confirm({
            title: 'Confirmar',
            message: 'Se eliminará la lista seleccionada y la información de candidatos . ¿Desea de continuar?'
        })
            .subscribe(respuestaOK => {
                if (respuestaOK) {
                    this.servicioEliminarLista(row);
                }
            })
    }

    servicioEliminarLista(row){
        this.loader.open();
        this.crudService.delete({
            listaId: row.id,
        }, Lista.EliminarLista)
            .pipe(
                takeUntil(this.unsuscribe$)
            ).subscribe(
            (resp) => {
                this.loader.close();
                this.util.mostrarMensaje(resp);
                this.getItems(true);
            },
            ()=>
            {
                this.loader.close();
            }
        )
    }

    irTabCandidatos(rowLista) {
        this.listaSelected.emit(rowLista)
    }

    limpiarArregloListas(){
        this.listas = [];
    }

}
