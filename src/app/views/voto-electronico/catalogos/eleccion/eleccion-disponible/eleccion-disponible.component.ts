import {Component, OnDestroy, OnInit} from '@angular/core';
import {CrudService} from '../../../../../shared/services/facade-services/crud.service';
import {ProcesoElectoral} from '../../../../constantes/apis';
import {ApiResponseMessageModel} from '../../../../../shared/models/voto-electronico/api-response-message.model';
import {UtilService} from '../../../../../shared/services/util/util.service';
import { Router} from '@angular/router';
import {AppLoaderService} from '../../../../../shared/services/app-loader/app-loader.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-eleccion-disponible',
  templateUrl: './eleccion-disponible.component.html',
  styleUrls: ['./eleccion-disponible.component.less']
})
export class EleccionDisponibleComponent implements OnInit, OnDestroy {

  procesosVigentes = [];
    private unsuscribe$ = new Subject<void>(); //marca como completado el observable

  constructor(
      private crudService: CrudService,
      private util: UtilService,
      private router: Router,
      private loader: AppLoaderService,
      ) { }

  ngOnInit(): void {
    this.obtenerEleccionesVigentesMedianteToken();
  }
    ngOnDestroy() {
        this.unsuscribe$.next();
        this.unsuscribe$.complete();
    }

  obtenerEleccionesVigentesMedianteToken(){
    this.loader.open();
    this.crudService.post({}, ProcesoElectoral.ObtenerProcesosVigentesByToken)
        .pipe(
            takeUntil(this.unsuscribe$)
        ).subscribe(
        (result: ApiResponseMessageModel) => {
          if (!this.util.existenErrores(result)) {
            if (result.responseList) {
              this.procesosVigentes = this.util.obtenerListaMensaje(result)

            }
            else {
              this.util.mostrarMensaje(result)
            }
          }
          this.loader.close();
          },()=>{
          this.loader.close();
        }
    )
  }

  irMesaDeVoto(procesoElectoralId){
    var idEncriptado = this.util.encriptar(procesoElectoralId)
    this.router.navigate(['/catalogos/mesa-voto',idEncriptado])
  }



}
