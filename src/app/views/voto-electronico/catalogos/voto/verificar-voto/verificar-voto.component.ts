import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {
  Auditoria,
  OpcionesBusquedaProcesosFinalizados
} from '../../../../constantes/variables-globales';
import {ProcesoElectoral} from '../../../../constantes/apis';
import {ApiResponseMessageModel} from '../../../../../shared/models/voto-electronico/api-response-message.model';
import {CrudService} from '../../../../../shared/services/facade-services/crud.service';
import {UtilService} from '../../../../../shared/services/util/util.service';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {PopupVerficarVotoComponent} from './popup-verficar-voto/popup-verficar-voto.component';
import {AppLoaderService} from '../../../../../shared/services/app-loader/app-loader.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-verificar-voto',
  templateUrl: './verificar-voto.component.html',
  styleUrls: ['./verificar-voto.component.less']
})
export class VerificarVotoComponent implements OnInit, OnDestroy {


  itemForm: FormGroup;
  procesosElectorales: [];
  today = new Date();
  parametroDeBusquedaSeleccionado = '';
  opcionesDeBusqueda = [];
  private unsuscribe$ = new Subject<void>(); //marca como completado el observable

  constructor(
      private fb: FormBuilder,
      private crudService: CrudService,
      private util: UtilService,
      private dialog: MatDialog,
      private loader: AppLoaderService,
  ) { }

  ngOnInit(): void {
    this.buildItemForm();
    this.opcionesDeBusqueda = OpcionesBusquedaProcesosFinalizados
    this.getItems();
  }
  /***
   * Elimina todas las subscribciones
   * de los observables
   */
  ngOnDestroy(): void {
    this.unsuscribe$.next(); // enviar el siguiente dato
    this.unsuscribe$.complete(); // marca como completado al observable
  }



  buildItemForm() {
    this.itemForm = this.fb.group({
      nombreProcesoElectoral: [''],
      estado: [Auditoria.ACTIVO],
      anioSeleccionado: [''],
    })
  }

  getItems() {
    this.loader.open();
    const data = this.itemForm.value;

    if (!data.estado) {
      data.estado = Auditoria.ACTIVO
    }
    this.crudService.post({
      parametroBusqueda1: data.nombreProcesoElectoral,
      parametroBusqueda5: data.anioSeleccionado,
    }, ProcesoElectoral.ObtenerProcesosFinalizadosByParametros)
        .pipe(
            takeUntil(this.unsuscribe$)
        ).subscribe(
        (result: ApiResponseMessageModel) => {

          if (!this.util.existenErrores(result)) {
            if (result.responseList) {
              this.procesosElectorales = this.util.obtenerListaMensaje(result).map(
                  (procesoElectoral: any) => {
                    //procesoElectoral.badge = procesoElectoral.estado == Auditoria.ACTIVO ? true : false;
                    // procesoElectoral.fechaInicio = new Date(procesoElectoral.fechaInicio)
                    // procesoElectoral.fechaFin = new Date(procesoElectoral.fechaFin)

                    // if (this.today > procesoElectoral.fechaFin) {
                    //
                    //   procesoElectoral.statusColor = BadgeEstados.Finalizado.color
                    //   procesoElectoral.statusText = BadgeEstados.Finalizado.text
                    // }
                    return procesoElectoral;
                  }
              );
            } else {
              this.util.mostrarMensaje(result)
            }
          }
          this.loader.close()
        },(response)=>{
          this.loader.close();
          this.util.existenErrores(response.error);
        }
    )
  }

  limpiar() {
    this.parametroDeBusquedaSeleccionado = '';
    this.itemForm.reset();
  }


  openPopUpVerificarVoto(row){

    let popUpVotoRealizado: MatDialogRef<any> = this.dialog.open(PopupVerficarVotoComponent, {
      width: '500px',
      disableClose:true,
      data: {procesoId:row?.id,}
    });
  }
}
