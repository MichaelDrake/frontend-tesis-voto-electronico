import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {UtilService} from '../../../../../../shared/services/util/util.service';
import {MesaVoto} from '../../../../../constantes/apis';
import {AppLoaderService} from '../../../../../../shared/services/app-loader/app-loader.service';
import {CrudServiceNoInterceptor} from '../../../../../../shared/services/facade-services/crud.service';
import {takeUntil} from 'rxjs/operators';
import {ToastrService} from 'ngx-toastr';

@Component({
    selector: 'app-popup-verficar-voto',
    templateUrl: './popup-verficar-voto.component.html',
    styleUrls: ['../verificar-voto.component.less']
})
export class PopupVerficarVotoComponent implements OnInit, OnDestroy {

    private unsuscribe$ = new Subject<void>(); //marca como completado el observable
    voto;
    listaOpciones?;
    contenido = '';
    procesoElectoralId;

    constructor(@Inject(MAT_DIALOG_DATA) public data: any,
                public dialogRef: MatDialogRef<PopupVerficarVotoComponent>,
                public util: UtilService,
                private loader: AppLoaderService,
                private crudService: CrudServiceNoInterceptor,
                private toastr: ToastrService,
    ) {

    }

    ngOnInit(): void {
        this.procesoElectoralId = this.data.procesoId;
    }

    /***
     * Elimina todas las subscribciones
     * de los observables
     */
    ngOnDestroy(): void {
        this.unsuscribe$.next(); // enviar el siguiente dato
        this.unsuscribe$.complete(); // marca como completado al observable
    }


    async  onSelectFile(e: any) {

        let archivoMascara = e.target.files[0]
        console.log(archivoMascara, 'asd')

        if (archivoMascara.type != 'text/plain') {
            this.toastr.error('Formato de comprobante no válido', 'Error');
        } else {
            let promise = this.getFileBlob(archivoMascara);
            var archivoCargado='';

            await promise.then((blob:string) => {
                archivoCargado = blob
            }).catch(err => {
                this.toastr.error('No se pudo cargar el archivo', 'Error');
                return;
            })

            await this.servicioVerificarVoto(archivoCargado);
        }
    }


    getFileBlob(file) {

        var reader = new FileReader();
        return new Promise(function (resolve, reject) {

            reader.onload = (function (theFile) {
                return function (e) {
                    resolve(e.target.result);

                };
            })(file);

            reader.readAsText(file);

        });
    }

    servicioVerificarVoto(archivoCargado) {
        console.log(archivoCargado,'desde el servicio')
        this.loader.open();
        this.crudService.post({
            mascara: archivoCargado.toString(),
            pelcId: btoa(this.procesoElectoralId)
        }, MesaVoto.VerificarVoto)
            .pipe(
                takeUntil(this.unsuscribe$)
            ).subscribe(
            response => {
                if (!this.util.existenErrores(response)) {
                    this.util.mostrarMensaje(response);
                    if (response.responseList) {
                        this.listaOpciones = this.util.obtenerListaMensaje(response, true)
                        this.listaOpciones.fechaVoto = new Date(this.listaOpciones.fechaVoto);
                    } else {
                        this.listaOpciones = [];
                    }
                }
                this.loader.close();
            },
            (error) => {
                this.loader.close();
                this.util.existenErrores(error.error)
                this.listaOpciones = null;
            }
        )
    }
}
