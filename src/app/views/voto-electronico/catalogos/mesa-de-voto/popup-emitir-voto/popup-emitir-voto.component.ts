import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormGroup} from '@angular/forms';
import {UtilService} from '../../../../../shared/services/util/util.service';
import {Subject} from 'rxjs';
import {Candidato} from '../../../../../shared/models/voto-electronico/api-response-message.model';
import {PaletaColoresPerzonalizados} from '../../../../constantes/variables-globales';

@Component({
  selector: 'app-popup-emitir-voto',
  templateUrl: './popup-emitir-voto.component.html',
  styleUrls: ['../mesa-de-voto.component.less' ]
})
export class PopupEmitirVotoComponent implements OnInit {
  itemForm: FormGroup;
  listavotos: Candidato[]=[];
  _estadoVoto = '';
  paletaColores = PaletaColoresPerzonalizados;

  constructor(
      @Inject(MAT_DIALOG_DATA) public data: any,
      public dialogRef: MatDialogRef<PopupEmitirVotoComponent>,
      private fb: FormBuilder,
      public util: UtilService,
  ) { }

  ngOnInit(): void {
    this.buildItemForm(this.data);
    this.listavotos = this.data.listaVotos;
    this._estadoVoto = this.data.estadoVoto;
  }

  buildItemForm(item) {
    this.itemForm = this.fb.group({});
  }


}
