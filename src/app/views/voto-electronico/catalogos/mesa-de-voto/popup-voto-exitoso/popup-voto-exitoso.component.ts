import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Subject} from 'rxjs';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {UtilService} from '../../../../../shared/services/util/util.service';
import {PaletaColoresPerzonalizados} from '../../../../constantes/variables-globales';

@Component({
  selector: 'app-popup-voto-exitoso',
  templateUrl: './popup-voto-exitoso.component.html',
  styleUrls: ['../mesa-de-voto.component.less' ]
})
export class PopupVotoExitosoComponent implements OnInit {

  itemForm: FormGroup;
  private unsuscribe$ = new Subject<void>(); //marca como completado el observable
  _mascara:string='';
  paletaColores = PaletaColoresPerzonalizados;

  constructor(
      @Inject(MAT_DIALOG_DATA) public data: any,
      public dialogRef: MatDialogRef<PopupVotoExitosoComponent>,
      private fb: FormBuilder,
      public util: UtilService,
  ) { }

  ngOnInit(): void {
    this.buildItemForm(this.data);
    this._mascara = this.data.mascara
  }

  buildItemForm(item) {
    this.itemForm = this.fb.group({});
  }




  /***
   * Elimina todas las subscribciones
   * de los observables
   */
  ngOnDestroy(): void {
    this.unsuscribe$.next(); // enviar el siguiente dato
    this.unsuscribe$.complete(); // marca como completado al observable
  }

}
