import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UtilService} from '../../../../shared/services/util/util.service';
import {CrudService, CrudServiceNoInterceptor} from '../../../../shared/services/facade-services/crud.service';
import {Cargo, Escanio, Lista, MesaVoto, Padron, ProcesoElectoral} from '../../../constantes/apis';
import {Candidato} from '../../../../shared/models/voto-electronico/api-response-message.model';
import {PaletaColoresPerzonalizados, VotoEstados} from '../../../constantes/variables-globales';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {PopupEmitirVotoComponent} from './popup-emitir-voto/popup-emitir-voto.component';
import {PopupVotoExitosoComponent} from './popup-voto-exitoso/popup-voto-exitoso.component';
import * as fileSaver from 'file-saver'
import {AppLoaderService} from '../../../../shared/services/app-loader/app-loader.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
    selector: 'app-mesa-de-voto',
    templateUrl: './mesa-de-voto.component.html',
    styleUrls: ['./mesa-de-voto.component.less']
})
export class MesaDeVotoComponent implements OnInit, OnDestroy {

    procesoElectoralId = ''
    procesoElectoralSeleccionado;
    listaCandidatos = [];
    listaDeEscanios = [];
    redirigiendo = false;
    votoEstado = {
        estado: VotoEstados.Blanco.estado,
        mensaje: VotoEstados.Blanco.mensaje,
        color: VotoEstados.Blanco.color,
    };

    paletaColores = PaletaColoresPerzonalizados;

    //Variables para graficar los votos
    //Pluripersonales
    candidatosSeleccionados: number[] = [];
    candidatosInformacionCompleta: Candidato[] = [];
    listasSeleccionadas: number[] = [];
    cargoVotante: string;
    private unsuscribe$ = new Subject<void>(); //marca como completado el observable
    constructor(private router: Router,
                private  route: ActivatedRoute,
                private util: UtilService,
                private crudService: CrudService,
                private crudServiceNoInterceptor: CrudServiceNoInterceptor,
                private dialog: MatDialog,
                private loader: AppLoaderService,
    ) {
    }

    ngOnInit(): void {

        this.procesoElectoralId = this.util.desencriptar(this.route.snapshot.paramMap.get('procesoElectoralId'));
        this.obtenerProcesoElectoralById(this.procesoElectoralId);
        this.obtenerEscaniosPorProcesoId(this.procesoElectoralId);

    }
    ngOnDestroy() {
        this.unsuscribe$.next();
        this.unsuscribe$.complete();
    }

    obtenerProcesoElectoralById(procesoElectoralId) {
        this.loader.open();
        this.crudService.post({
            Id: procesoElectoralId
        }, ProcesoElectoral.ObtenerProcesoElectoralById)
            .pipe(
                takeUntil(this.unsuscribe$)
            ).subscribe(response => {
            if (!this.util.existenErrores(response)) {

                if (response.responseList) {
                    this.procesoElectoralSeleccionado = response.responseList[0];
                    if (this.procesoElectoralSeleccionado.tipoEleccion == 'Unipersonal') {
                        this.obtenerCargoToken();
                    }
                    this.obtenerlistasCandidatosPorProcesoId(this.procesoElectoralId, true)
                }
            }
        }, () => {
            this.loader.close();
        })
    }

    /*
    * Servicio para obtener todas las listas y candidatos del proceso electoral actual.
    * */
    obtenerlistasCandidatosPorProcesoId(procesoElectoralId, loaderIsActive) {
        if (!loaderIsActive) {
            this.loader.open();
        }
        this.crudService.post({
            procesoElectoralId: procesoElectoralId
        }, Lista.ObtenerListasCandidatosByProcesoId)
            .pipe(
                takeUntil(this.unsuscribe$)
            ).subscribe(response => {
            if (!this.util.existenErrores(response)) {

                if (response.responseList) {
                    this.listaCandidatos = this.util.obtenerListaMensaje(response)
                } else {
                    this.util.mostrarMensaje(response)
                }
                    this.loader.close();

            }
        }, () => {
                this.loader.close()
        })
    }

    /*
    * Servicio para obtener los escanios a elegir del proceso electoral actual.
    * */
    obtenerEscaniosPorProcesoId(procesoElectoralId) {
        this.crudService.post({
            Id: procesoElectoralId
        }, Escanio.ObtenerEscaniosDisponiblesMedianteProcesoElectoralId)
            .pipe(
                takeUntil(this.unsuscribe$)
            )
            .subscribe(response => {
                if (!this.util.existenErrores(response)) {
                    if (response.responseList) {
                        this.listaDeEscanios = this.util.obtenerListaMensaje(response)
                    } else {
                        this.util.mostrarMensaje(response)
                    }
                }
            })
    }

    obtenerCargoToken() {
        this.crudService.post({}, Cargo.ObtenerCargoToken)
            .pipe(
                takeUntil(this.unsuscribe$)
            )
            .subscribe(response => {
                this.cargoVotante = this.util.obtenerListaMensaje(response, true)?.id;
            })
    }

    seleccionarCandidato(candidato, lista) {

        // Primer If deselecciona el cuadro de voto
        if (this.candidatosSeleccionados.includes(candidato.id)) {
            //Borrar candidato de los arrays de candidatos
            var indiceElemento = this.candidatosSeleccionados.indexOf(candidato.id)
            this.candidatosSeleccionados.splice(indiceElemento, 1)
            //Borrar candidato del arreglo de candidatos con informacion completa
            var indiceObjeto;
            this.candidatosInformacionCompleta.forEach((item: Candidato, indiceActual) => {
                if (item.candidatoId === candidato.id) {
                    indiceObjeto = indiceActual;
                }
            })
            this.candidatosInformacionCompleta.splice(indiceObjeto, 1)

        }
        // Else, selecciona el cuadro de voto
        else {
            this.candidatosSeleccionados.push(candidato.id);
            var candidatoObject: Candidato = {
                candidatoId: candidato.id,
                listaId: candidato.listaId,
                escanioId: candidato.escanioId,
                nombreCandidato: candidato.nombreCandidato,
                nombreEscanio: candidato.nombreEscanio,
                nombreLista: candidato.nombreLista,
            }
            this.candidatosInformacionCompleta.push(candidatoObject)
        }
        //validar si los candidatos son elegidos en plancha

        if (this.procesoElectoralSeleccionado?.tipoEleccion == 'Pluripersonal') {
            this.validarCandidatosEnPlanchaDeLista(lista);
            this.validarVotoPluripersonal();
        }

    }

    seleccionarListaConCandidatos(lista) {


        if (this.listasSeleccionadas.includes(lista.id)) {
            var indiceElemento = this.listasSeleccionadas.indexOf(lista.id)
            this.listasSeleccionadas.splice(indiceElemento, 1)
            this.quitarCandidatosDeLista(lista);
        } else {
            this.listasSeleccionadas.push(lista.id)
            lista.candidatos.forEach(candidato => {
                if (!this.candidatosSeleccionados.includes(candidato.id)) {
                    this.candidatosSeleccionados.push(candidato.id)
                    var candidatoObject: Candidato = {
                        candidatoId: candidato.id,
                        listaId: candidato.listaId,
                        escanioId: candidato.escanioId,
                        nombreCandidato: candidato.nombreCandidato,
                        nombreEscanio: candidato.nombreEscanio,
                        nombreLista: candidato.nombreLista,
                    }
                    this.candidatosInformacionCompleta.push(candidatoObject)
                }
            })
        }

        //Para pintar cuadro de lista seleccionada en completo.
        this.validarCandidatosEnPlanchaDeLista(lista);
        //Para el label de estados del voto
        this.validarVotoPluripersonal();
    }

    seleccionarListaUnipersonal(lista) {

        if (this.listasSeleccionadas.includes(lista?.id)) {
            var indiceElemento = this.listasSeleccionadas.indexOf(lista.id)
            this.listasSeleccionadas.splice(indiceElemento, 1)
            this.quitarCandidatosDeLista(lista);
        } else {
            this.listasSeleccionadas.push(lista.id);
            lista.candidatos.map(
                candidato => {
                    this.seleccionarCandidato(candidato, lista)
                }
            )
        }
        this.validarVotoUnipersonal();
    }

    /*
    * Esta funcion sirve para graficar el cuadro de lista, si se seleccionan todos
    * los candidatos de la misma.
    * */
    validarCandidatosEnPlanchaDeLista(lista) {
        var candidatosEnPlancha = true;
        var candidatosDeLista = lista.candidatos.map(candidato => {
            return candidato.id
        })

        candidatosDeLista.forEach(
            (candidatoId) => {
                if (!this.candidatosSeleccionados.includes(candidatoId)) {
                    candidatosEnPlancha = false;
                }
            }
        )


        if (candidatosEnPlancha) {

            var indiceElemento = null;

            indiceElemento = this.listasSeleccionadas.indexOf(lista.id)

            if (indiceElemento == -1) {
                this.listasSeleccionadas.push(lista.id);
            }

        } else {
            var indiceElemento = null;
            indiceElemento = this.listasSeleccionadas.indexOf(lista.id)
            if (indiceElemento > -1) {
                this.listasSeleccionadas.splice(indiceElemento, 1)
            }
        }
    }

    quitarCandidatosDeLista(lista) {
        var candidatosDeLista = lista.candidatos.map(candidato => {
            return candidato.id
        })

        candidatosDeLista.forEach(
            candidatoId => {
                var indiceElemento = this.candidatosSeleccionados.indexOf(candidatoId)
                this.candidatosSeleccionados.splice(indiceElemento, 1);
                var indiceObjeto;
                this.candidatosInformacionCompleta.forEach((item: Candidato, indiceActual) => {
                    if (item.candidatoId === candidatoId) {
                        indiceObjeto = indiceActual;
                    }
                })
                this.candidatosInformacionCompleta.splice(indiceObjeto, 1)

            }
        )


    }


    /* Empieza logica para emitir el voto*/

    emitirVoto() {

        this.loader.open();
        var votoJSON = this.validarVoto();

        //Genero claves para encriptar mi voto
        var keyAes = this.util.generarKeyAes();
        var iv = this.util.generarVectorInicializacionAes();
        //Encriptar el voto con las claves
        var votoEncriptadoAESObjeto = this.util.encriptarVotoConAes(votoJSON, keyAes, iv);
        //Generar mascara
        var mascara = this.util.generarMascara(votoEncriptadoAESObjeto.toString())

        this.autorizarVoto(votoEncriptadoAESObjeto, mascara)
            .pipe(
                takeUntil(this.unsuscribe$)
            )
            .subscribe(response => {
                if (!this.util.existenErrores(response)) {
                    if (response.responseList) {
                        var votoFirmado = response.responseList[0].votoFirmado;
                        // Llamar al servicio para guardar el voto.
                        this.guardarVoto(votoFirmado,
                            votoEncriptadoAESObjeto.toString(),
                            keyAes,
                            iv,
                            mascara.toString(),
                        )
                            .pipe(
                                takeUntil(this.unsuscribe$)
                            )
                            .subscribe(
                                response => {
                                    if (!this.util.existenErrores(response)) {
                                        this.util.mostrarMensaje(response)
                                        try {
                                            this.descargarMascara(mascara);
                                            let popUpVotoRealizado: MatDialogRef<any> = this.dialog.open(PopupVotoExitosoComponent, {
                                                width: '500px',
                                                disableClose:true,
                                                data: {
                                                    mascara: mascara
                                                }
                                            });
                                            popUpVotoRealizado.afterClosed().subscribe(result => {
                                                this.redirigiendo = true
                                                this.router.navigate(['/catalogos/eleccion-disponible'])
                                            });
                                        } catch (err) {
                                            alert(err)
                                        }
                                    }
                                    this.loader.close();
                                }, (response) => {
                                    this.loader.close();
                                    this.util.existenErrores(response.error)
                                }
                            )
                    } else {
                        this.util.mostrarMensaje(response)
                    }
                    this.loader.close();
                }

            }, (response) => {
                this.loader.close();
                this.util.mostrarMensaje(response.error)
            })
    }

    /*
    * Mensaje de confirmacion para el voto
    * */
    confirmarVoto() {
        let popUpConfirmarvoto: MatDialogRef<any> = this.dialog.open(PopupEmitirVotoComponent, {
            width: '400px',
            disableClose:true,
            data: {
                listaVotos: this.candidatosInformacionCompleta,
                estadoVoto: this.votoEstado.estado,
            }
        });
        popUpConfirmarvoto.afterClosed().subscribe(result => {
            if (result) {
                this.emitirVoto();
            }
        });
    }

    /*
    Date: 1/12/20
    Esta funcion crea el cuerpo de la peticion para emitir el voto
     */
    validarVoto() {
        if (this.procesoElectoralSeleccionado?.tipoEleccion == 'Unipersonal') {
            return this.validarVotoUnipersonal();
        } else {
            return this.validarVotoPluripersonal();
        }
    }

    validarVotoUnipersonal() {
        var voto;
        if (this.listasSeleccionadas.length == 0) {
            voto = []
            this.votoEstado.estado = VotoEstados.Blanco.estado
            this.votoEstado.mensaje = VotoEstados.Blanco.mensaje
            this.votoEstado.color = VotoEstados.Blanco.color
        } else if (this.listasSeleccionadas.length > 1) {
            voto = null
            this.votoEstado.estado = VotoEstados.Nulo.estado
            this.votoEstado.mensaje = VotoEstados.Nulo.mensajeListasExcedidas
            this.votoEstado.color = VotoEstados.Nulo.color
        } else {
            voto = this.candidatosInformacionCompleta
            this.votoEstado.estado = VotoEstados.Valido.estado
            this.votoEstado.color = VotoEstados.Valido.color
            this.votoEstado.mensaje = VotoEstados.Valido.mensaje
        }
        return voto;
    }

    validarVotoPluripersonal() {
        var voto;
        if (this.candidatosSeleccionados.length == 0) {
            voto = []
            this.votoEstado.estado = VotoEstados.Blanco.estado
            this.votoEstado.mensaje = VotoEstados.Blanco.mensaje
            this.votoEstado.color = VotoEstados.Blanco.color
        } else if (this.candidatosSeleccionados.length > this.listaDeEscanios.length) {
            voto = null
            this.votoEstado.estado = VotoEstados.Nulo.estado
            this.votoEstado.mensaje = VotoEstados.Nulo.mensajeOpcionesExcedidas
            this.votoEstado.color = VotoEstados.Nulo.color
        } else {
            voto = this.candidatosInformacionCompleta
            this.votoEstado.estado = VotoEstados.Valido.estado
            this.votoEstado.color = VotoEstados.Valido.color
            this.votoEstado.mensaje = VotoEstados.Valido.mensaje
        }
        return voto;
    }

    existenEscaniosRepetidos() {
        var flag = null;
        flag = !this.candidatosInformacionCompleta.every(
            (valor, indice, lista) => {
                return this.esPrimero(valor.escanioId, indice, lista)
            }
        )
        return flag
    }

    esPrimero(valor, indice, lista) {
        var listaEscaniosId = lista.map((candidatoItem: Candidato) => {
            return candidatoItem.escanioId
        })
        return (listaEscaniosId.indexOf(valor) === indice);
    }

    /**
     * Servicio para autorizar el voto
     */
    autorizarVoto(votoEncriptadoAESObjeto, mascara) {

        return this.crudService.post({
            VotoCifrado: votoEncriptadoAESObjeto.toString(),
            Mascara: mascara.toString(),
            pelcid: this.procesoElectoralId,
        }, Padron.ValidarVotoUsuario);

    }

    /**
     * Servicio para guardar el voto
     */
    guardarVoto(votoFirmado, votoEncriptadoAESObjeto, keyAes, iv, mascara) {

        return this.crudServiceNoInterceptor.post({
            votoFirmado: votoFirmado,
            votoCifradoAes: votoEncriptadoAESObjeto,
            key: keyAes,
            iv: iv,
            h: mascara,
            procesoId: btoa(this.procesoElectoralId),
            cargoId: this.cargoVotante,
        }, MesaVoto.EmitirVoto);
    }

    /**
     * Servicio para guardar la mascar en la pc del usuario.
     */
    descargarMascara(mascara: string) {
        const fecha = new Date();
        var blob = new Blob([btoa(mascara)], {type: 'text.plain;charset=utf-8'});
        //saveAs(blob, "comprobante de voto")
        fileSaver.saveAs(blob, `EVOTE-EPN-AUTHENTICATED-RECEIPT-0101${Number(fecha.getDate())}SIGNED`)

    }
}
