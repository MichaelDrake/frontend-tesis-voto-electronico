import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatCardModule} from '@angular/material/card';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatIconModule} from '@angular/material/icon';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {CatalogosRoutingModule} from './catalogos.routing';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import {SharedModule} from '../../../shared/shared.module';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {MatSelectModule} from '@angular/material/select';
import { ProcesoElectoralComponent } from './administrar-proceso/tab-proceso-electoral/proceso-electoral.component';
import {MatChipsModule} from '@angular/material/chips';
import { PopupProcesoElectoralComponent } from './administrar-proceso/tab-proceso-electoral/popup-proceso-electoral/popup-proceso-electoral.component';
import {MatTableModule} from '@angular/material/table';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { PopupListaComponent } from './administrar-proceso/tab-de-lista/popup-lista/popup-lista.component';
import {OwlDateTimeIntl, OwlDateTimeModule, OwlNativeDateTimeModule} from 'ng-pick-datetime';
import {AdministrarProcesoComponent} from './administrar-proceso/administrar-proceso.component';
import {MatTabsModule} from '@angular/material/tabs';
import {MatDividerModule} from '@angular/material/divider';
import { TabDeListaComponent } from './administrar-proceso/tab-de-lista/tab-de-lista.component';
import {MatGridListModule} from '@angular/material/grid-list';
import { TabCandidatosComponent } from './administrar-proceso/tab-candidatos/tab-candidatos.component';
import {FileUploadModule} from 'ng2-file-upload';
import {Ng2ImgMaxModule} from 'ng2-img-max';
import {ColorPickerModule} from 'ngx-color-picker';
import { TabImportacionPadronComponent } from './administrar-proceso/tab-importacion-padron/tab-importacion-padron.component';
import {PopupErrorMensajeComponent} from './administrar-proceso/tab-importacion-padron/popup-error/popup-error.component';
import { EleccionDisponibleComponent } from './eleccion/eleccion-disponible/eleccion-disponible.component';
import { MesaDeVotoComponent } from './mesa-de-voto/mesa-de-voto.component';
import {MatRadioModule} from '@angular/material/radio';
import { PopupEmitirVotoComponent } from './mesa-de-voto/popup-emitir-voto/popup-emitir-voto.component';
import { PopupVotoExitosoComponent } from './mesa-de-voto/popup-voto-exitoso/popup-voto-exitoso.component';
import {PopupCandidatoComponent} from './administrar-proceso/tab-candidatos/popup-candidato/popup-candidato.component';
import { VerificarVotoComponent } from './voto/verificar-voto/verificar-voto.component';
import { PopupVerficarVotoComponent } from './voto/verificar-voto/popup-verficar-voto/popup-verficar-voto.component';
import { PerfilComponent } from './perfil/perfil.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatTooltipModule} from '@angular/material/tooltip';
import { PopupEmpadronadoComponent } from './administrar-proceso/tab-importacion-padron/popup-empadronado/popup-empadronado.component';





export class DefaultIntl extends OwlDateTimeIntl {

    /** A label for the cancel button */
    cancelBtnLabel= 'Cancelar';

    /** A label for the set button */
    setBtnLabel= 'Guardar';

};

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MatProgressBarModule,
        MatButtonModule,
        MatInputModule,
        MatCardModule,
        MatCheckboxModule,
        MatIconModule,
        FlexLayoutModule,
        CatalogosRoutingModule,
        SharedModule,
        NgxDatatableModule,
        MatSelectModule,
        MatChipsModule,
        MatTableModule,
        MatDatepickerModule,
        MatTabsModule,
        MatDividerModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        MatGridListModule,
        FileUploadModule,
        Ng2ImgMaxModule,
        ColorPickerModule,
        MatRadioModule,
        MatDialogModule,
        MatTooltipModule,
    ],
    declarations: [
        ProcesoElectoralComponent,
        PopupProcesoElectoralComponent,
        PopupListaComponent,
        PopupCandidatoComponent,
        AdministrarProcesoComponent,
        TabDeListaComponent,
        TabCandidatosComponent,
        TabImportacionPadronComponent,
        PopupErrorMensajeComponent,
        EleccionDisponibleComponent,
        MesaDeVotoComponent,
        PopupEmitirVotoComponent,
        PopupVotoExitosoComponent,
        VerificarVotoComponent,
        PopupVerficarVotoComponent,
        PerfilComponent,
        PopupEmpadronadoComponent,
       ],
    entryComponents: [
        PopupProcesoElectoralComponent,
        PopupErrorMensajeComponent
    ],
    providers:[
        {provide: OwlDateTimeIntl, useClass: DefaultIntl},
    ],
})


export class CatalogoModule {
}

