import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Usuario} from '../../../constantes/apis';
import {ApiResponseMessageModel} from '../../../../shared/models/voto-electronico/api-response-message.model';
import {CrudService} from '../../../../shared/services/facade-services/crud.service';
import {UtilService} from '../../../../shared/services/util/util.service';
import {Ng2ImgMaxService} from 'ng2-img-max';
import {AppLoaderService} from '../../../../shared/services/app-loader/app-loader.service';
import {User} from '../../../../shared/models/user.model';
import {JwtAuthService} from '../../../../shared/services/auth/jwt-auth.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.less']
})
export class PerfilComponent implements OnInit, OnDestroy {
  formData = {};
  console = console;
  itemForm: FormGroup;
  usuario ;
  persona = {};
  private unsuscribe$ = new Subject<void>(); //marca como completado el observable
  constructor(
      private crudService: CrudService,
      private util: UtilService,
      private fb: FormBuilder,
      private ng2ImgMaxService: Ng2ImgMaxService,
      private loader: AppLoaderService,
      public jwtAuth: JwtAuthService
  ) { }

  ngOnInit(): void {

    this.buildItemForm();
    this.obtenerInformacionUsuario(false);

  }

  /***
   * Elimina todas las subscribciones
   * de los observables
   */
  ngOnDestroy(): void {
    this.unsuscribe$.next(); // enviar el siguiente dato
    this.unsuscribe$.complete(); // marca como completado al observable
  }

  buildItemForm() {
    this.itemForm = this.fb.group({
      imagenUsuario: [ '',Validators.required],
    })
  }


  obtenerInformacionUsuario(existeLoader){
    if(!existeLoader)this.loader.open();
    this.crudService.post({ }, Usuario.ObtenerUsuarioMedianteToken)
        .pipe(
            takeUntil(this.unsuscribe$)
        ).subscribe(
        (response: ApiResponseMessageModel) => {

          if (!this.util.existenErrores(response)) {
            if (response.responseList) {
              this.usuario = this.util.obtenerListaMensaje(response,true);
            }
          }
          this.loader.close();
        },()=>{
          this.loader.close();
        }
    )
  }

  actualizarUsuario(){
    this.loader.open();

    this.crudService.put({
      fotoObjeto: this.itemForm.controls['imagenUsuario'].value,
      idUsuario: this.usuario.idUsuario,
    }, Usuario.ActualizarUsuarioWeb)
        .pipe(
            takeUntil(this.unsuscribe$)
        ).subscribe(
        (response: ApiResponseMessageModel) => {

          this.util.mostrarMensaje(response)
          if (response.responseList != null) {
            let usuarioActualizado = this.util.obtenerListaMensaje(response,true);
            let usuario: User = {
              displayName: usuarioActualizado.objetoPersona.nombreUno+' '+usuarioActualizado.objetoPersona.apellidoUno,
              imagen: usuarioActualizado.fotoUrl
            }
            this.jwtAuth.setUserAndToken(usuarioActualizado.token, usuario,true);
            this.obtenerInformacionUsuario(true);
          }
          this.loader.close();
        },()=>{
          this.loader.close();
        }
    )
  }


  /**
   * maneja las imagenes que se cargan
   * @param event
   */
  onSelectFile(event) {

    //console.log(event)
    const extensionImg = event.target.files[0];
    // if (!this.isNew) {
    //     this.listaImagenes = [];
    // }
    if (event.target.files && event.target.files[0]) {
      // this.cargandoImagen = true
      let file = event.target.files[0];

      this.ng2ImgMaxService.resizeImage(file, 720
          , 450).subscribe(result => {
        var reader = new FileReader();

        reader.onload = (event: any) => {
          const logo = {
            base64: event.target.result,
            tipo: extensionImg.type,
            extension: '.' + extensionImg.name.split('.')[1],
          };
          // this.cargandoImagen = false;
          this.itemForm.controls['imagenUsuario'].setValue(logo);
          this.itemForm.controls['imagenUsuario'].updateValueAndValidity();

        };
        reader.readAsDataURL(result);

      })
    }
  }

}
